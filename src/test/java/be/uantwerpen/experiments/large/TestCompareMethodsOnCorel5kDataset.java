package be.uantwerpen.experiments.large;

import java.io.File;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import be.uantwerpen.lcif_knn.LCIFExperimenter;
import be.uantwerpen.lcif_knn.LCIFExperimenter.LCIFResult;
import be.uantwerpen.util.util.IOUtils;
import be.uantwerpen.util.util.MathUtils;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCompareMethodsOnCorel5kDataset extends TestCompareMethodsOnMedical1kDataset{

	public TestCompareMethodsOnCorel5kDataset(){
		File datadir =  new File("src/main/resources/multi-label-datasets/");
		this.inputTrainArff = new File(datadir,"corel5k-train-sparse.arff");
		this.inputTestArff = new File(datadir,"corel5k-test-sparse.arff");
		this.labels = new File(datadir,"corel5k-sparse.xml");
		this.trainLibsvm  = new File(datadir, "corel5k-train-sparse.libsvm");
		this.testLibsvm = new File(datadir, "corel5k-test-sparse.libsvm");
		this.outputGS = new File("temp/corel5k-gs.csv");
		this.outputGS_mulan = new File("temp/corel5k-gs-mulan.csv");
	}

	boolean runMulan = true;
	
	@Test
	public void runLCIFGridSearch() throws Exception{
		LCIFExperimenter experimenter = new LCIFExperimenter();
		if(TestLCIF2.LCIF2)
			experimenter.setLCIF2Modus();
		int[] kRowRange = MathUtils.makeRange(1, 31, 2);
		int[] kFeatureRange = MathUtils.makeRange(1, 31, 2);
		double[] lambdaRange = MathUtils.makeRangeIncl(0.0, 1.0, 0.1); //e.g. 0 == pure Row-based... 1 == pure Feature-based
		List<LCIFResult> results = experimenter.runLCIFGridSearchFolded(trainLibsvm, 10, kRowRange, kFeatureRange, lambdaRange);	
		experimenter.reportParameters(trainLibsvm, testLibsvm, results, outputGS);
		IOUtils.printHead(outputGS);	
	}
	
	@Test
	public void runLCIFOuterLoop() throws Exception{
		LCIFExperimenter experimenter = new LCIFExperimenter();
		if(TestLCIF2.LCIF2)
			experimenter.setLCIF2Modus();
		List<LCIFResult> resultsGS = experimenter.parseResults(outputGS);
		List<LCIFResult> outerResults = experimenter.runOuterLoop(trainLibsvm, testLibsvm, resultsGS);
		for(LCIFResult res: outerResults){
			System.out.format("LCIF;kRow=%d;kFeature=%d;lambda=%.3f;t=%.3f, %s=%.3f\n", 
					res.kRow, res.kFeature, res.lambda, res.optimalTreshold, res.evaluationValues.get(0).name, res.evaluationValues.get(0).value);
		}
	}
	

	
}

