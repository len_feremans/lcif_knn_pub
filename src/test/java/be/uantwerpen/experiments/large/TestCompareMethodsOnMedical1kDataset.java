package be.uantwerpen.experiments.large;

import java.io.File;
import java.util.List;

import mulan.classifier.MultiLabelLearner;
import mulan.data.MultiLabelInstances;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import be.uantwerpen.lcif_knn.LCIFExperimenter;
import be.uantwerpen.lcif_knn.LCIFExperimenter.LCIFResult;
import be.uantwerpen.mulan_wrapper.MulanExperimenter;
import be.uantwerpen.util.model.MultiKeyMap2;
import be.uantwerpen.util.model.Pair;
import be.uantwerpen.util.util.IOUtils;
import be.uantwerpen.util.util.MathUtils;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCompareMethodsOnMedical1kDataset {

	protected File inputTrainArff = new File("src/main/resources/multi-label-datasets/medical-train.arff");
	protected File inputTestArff = new File("src/main/resources/multi-label-datasets/medical-test.arff");
	protected File labels = new File("src/main/resources/multi-label-datasets/medical.xml");
	protected File trainLibsvm  = new File("src/main/resources/multi-label-datasets/medical-train.libsvm");
	protected File testLibsvm = new File("src/main/resources/multi-label-datasets/medical-test.libsvm");
	protected File outputGS = new File("temp/medical-gs.csv");
	protected File outputGS_mulan = new File("temp/medical-gs-mulan.csv");
	
	@Test
	public void runLCIFGridSearch() throws Exception{
		LCIFExperimenter experimenter = new LCIFExperimenter();
		if(TestLCIF2.LCIF2)
			experimenter.setLCIF2Modus();
		int[] kRowRange = MathUtils.makeRange(1, 31, 2);
		int[] kFeatureRange = MathUtils.makeRange(1, 31, 2);
		double[] lambdaRange = MathUtils.makeRangeIncl(0.0, 1.0, 0.01); //e.g. 0 == pure Row-based... 1 == pure Feature-based
		List<LCIFResult> results = experimenter.runLCIFGridSearchFolded(trainLibsvm, 10, kRowRange, kFeatureRange, lambdaRange);	
		experimenter.reportParameters(trainLibsvm, testLibsvm, results, outputGS);
		IOUtils.printHead(outputGS,100);	
	}
	
	@Test
	public void runLCIFOuterLoop() throws Exception{
		LCIFExperimenter experimenter = new LCIFExperimenter();
		if(TestLCIF2.LCIF2)
			experimenter.setLCIF2Modus();
		List<LCIFResult> resultsGS = experimenter.parseResults(outputGS);
		List<LCIFResult> outerResults = experimenter.runOuterLoop(trainLibsvm, testLibsvm, resultsGS);
		for(LCIFResult res: outerResults){
			System.out.format("LCIF;kRow=%d;kFeature=%d;lambda=%.3f;t=%.3f, %s=%.3f\n", 
					res.kRow, res.kFeature, res.lambda, res.optimalTreshold, res.evaluationValues.get(0).name, res.evaluationValues.get(0).value);
		}
	}

	@Test
	public void runMulanGridSearch() throws Exception{
		MulanExperimenter mulanExperimenter = new MulanExperimenter();
		int[] kRowRange = MathUtils.makeRange(1, 31, 2);
		MultiKeyMap2<String, String, String> results = mulanExperimenter.runGridSearch(inputTrainArff, inputTestArff,labels, kRowRange, 10);
		mulanExperimenter.reportParameters(results, outputGS_mulan);
		IOUtils.printHead(outputGS_mulan);	
	}
	
	@Test
	public void runMulanOuterLoop() throws Exception{
		MulanExperimenter mulanExperimenter = new MulanExperimenter();
		MultiLabelInstances dataset =  new MultiLabelInstances(inputTrainArff.getAbsolutePath(), labels.getAbsolutePath());
		List<Pair<MultiLabelLearner, String>> learners = mulanExperimenter.parseLearners(dataset, outputGS_mulan);
		mulanExperimenter.runOuter(this.inputTrainArff, this.inputTestArff, this.labels, learners);
	}

}
