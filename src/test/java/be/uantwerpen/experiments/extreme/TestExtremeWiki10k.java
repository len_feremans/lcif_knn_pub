package be.uantwerpen.experiments.extreme;

import java.io.File;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import be.uantwerpen.experiments.large.TestLCIF2;
import be.uantwerpen.lcif_knn.LCIFExperimenter;
import be.uantwerpen.lcif_knn.LCIFExperimenter.LCIFResult;
import be.uantwerpen.util.util.IOUtils;
import be.uantwerpen.util.util.MathUtils;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestExtremeWiki10k {
	
	protected File homedir = new File("./");
	protected File trainLibsvm  = new File(homedir, "src/main/resources/multi-label-datasets/extreme/wiki10_train.txt");
	protected File testLibsvm = new File(homedir, "src/main/resources/multi-label-datasets/extreme/wiki10_test.txt");
	protected File outputGS  =  new File(homedir, "temp/wiki10k-gs.csv");
	
	public TestExtremeWiki10k(){
	
	}
	
	@Test
	public void runLCIFGridSearch() throws Exception{
		LCIFExperimenter experimenter = new LCIFExperimenter();
		if(TestLCIF2.LCIF2)
			experimenter.setLCIF2Modus();
		int[] kRowRange = MathUtils.makeRange(1, 31, 2);
		int[] kFeatureRange = MathUtils.makeRange(1, 31, 2);
		double[] lambdaRange = MathUtils.makeRangeIncl(0.0, 1.0, 0.1); //e.g. 0 == pure Row-based... 1 == pure Feature-based
		List<LCIFResult> results = experimenter.runLCIFGridSearchFolded(trainLibsvm, 10, kRowRange, kFeatureRange, lambdaRange);	
		experimenter.reportParameters(trainLibsvm, testLibsvm, results, outputGS);
		IOUtils.printHead(outputGS);	
	}
	
	@Test
	public void runLCIFOuterLoop() throws Exception{
		LCIFExperimenter experimenter = new LCIFExperimenter();
		if(TestLCIF2.LCIF2)
			experimenter.setLCIF2Modus();
		List<LCIFResult> resultsGS = experimenter.parseResults(outputGS);
		List<LCIFResult> outerResults = experimenter.runOuterLoop(trainLibsvm, testLibsvm, resultsGS);
		for(LCIFResult res: outerResults){
			System.out.format("LCIF;kRow=%d;kFeature=%d;lambda=%.3f;t=%.3f, %s=%.3f\n", 
					res.kRow, res.kFeature, res.lambda, res.optimalTreshold, res.evaluationValues.get(0).name, res.evaluationValues.get(0).value);
		}
	}

}
