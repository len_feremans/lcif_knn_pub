package be.uantwerpen.experiments.extreme;

import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;

import be.uantwerpen.lcif_knn.FeatureKNNCache;
import be.uantwerpen.lcif_knn.FeatureKNNPredictions;
import be.uantwerpen.lcif_knn.FeatureKNNSearch;
import be.uantwerpen.lcif_knn.LCIFData;
import be.uantwerpen.lcif_knn.LCIFEvaluation;
import be.uantwerpen.lcif_knn.LCIFEvaluation.EvaluationMetricValue;
import be.uantwerpen.lcif_knn.LCIFExperimenter;
import be.uantwerpen.lcif_knn.LCIFLinearCombination;
import be.uantwerpen.lcif_knn.LCIFPredictions;
import be.uantwerpen.lcif_knn.RowKNNCache;
import be.uantwerpen.lcif_knn.RowKNNPredictions;
import be.uantwerpen.lcif_knn.RowKNNSearch;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.util.CollectionUtils;
import be.uantwerpen.util.util.IOUtils;
import be.uantwerpen.util.util.MathUtils;

public class TestExtremeWikiLSHTC2300K{

	LCIFExperimenter experimenter = new LCIFExperimenter();
	RowKNNSearch rowKNNSearch = new RowKNNSearch();
	RowKNNPredictions rowKNNPredictions = new RowKNNPredictions();
	FeatureKNNSearch featureKNNSearch = new FeatureKNNSearch();
	FeatureKNNPredictions featureKNNPredictions = new FeatureKNNPredictions();
	LCIFLinearCombination lcifLinearCombination = new LCIFLinearCombination();
	LCIFEvaluation evaluator = new LCIFEvaluation();

	//PARAMETERS
	//FOR HPC -> CHANGE
	File homedir = new File("./");
	File trainLibsvm = new File(homedir, "src/main/resources/multi-label-datasets/extreme/wikiLSHTC_train.txt");
	File testLibsvm = new File(homedir, "src/main/resources/multi-label-datasets/extreme/wikiLSHTC_test.txt");
	int maxK = 30;
	int maxK2 = 15;
	int[] kRowRange = MathUtils.makeRange(1, maxK+1, 5);
	int[] kFeatureRange = MathUtils.makeRange(1, maxK2+1, 3); 
	double[] lambdaRange = MathUtils.makeRangeIncl(1.0, 0.0, -0.2);

	boolean debug = false;
	boolean reUseCache = false;
	
	@Test
	public void runRowKnn() throws Exception{	
		//RUN
		LCIFData data = new LCIFData();
		data.loadData(trainLibsvm, true);
		//for testing: take first test example
		if(debug){
			File test= IOUtils.copyPartOfFile(testLibsvm, 1, 3);
			testLibsvm  = test;
			kRowRange = new int[]{2};
			kFeatureRange = new int[]{2};
			lambdaRange = new double[]{1.0,0.5,0.0};
		}
		data.loadData(testLibsvm, false);
		data.makeIndex();
		//data.printDataStats();
		
		FileWriter writer = new FileWriter("./temp/results-csv-" + data.getDatasetName() + ".csv", true);
		//knn search
		Timer timerS = new Timer("kRow search " + maxK);
		//TEMP!
		RowKNNCache cache = new RowKNNCache();
		if(reUseCache)
			cache.load(new File("/tmp/all-similar-knn1176387747947136412cache"));
		else //takes 13 hours on LSHTC
			cache = rowKNNSearch.computeAllSimilarRowsMultiThreaded(data, maxK);
		long milisS = timerS.end();
		writer.write(String.format("knn-search: %d\n", milisS));

		//knn predictions
		HashMap<Integer,LCIFPredictions> rowBasedPredictions = new HashMap<>();
		for(int kRow: kRowRange){ //1 minutes
			Timer timer = new Timer("kRow " + kRow);
			LCIFPredictions predictionsData = rowKNNPredictions.makePredictionAllTestRows(data, cache, kRow);
			long milis = timer.end();
			if(debug)
				System.out.println(predictionsData);
			//evaluate
			rowBasedPredictions.put(kRow,predictionsData);
			double optimalThreshold = evaluator.computeOptimalSingleThresholdLabelCardinality(data, predictionsData);  //cheating slightly.. optimized on test set 
			if(debug){
				System.out.println("threshold: " + optimalThreshold);
				//System.out.println(CollectionUtils.join(predictionsData.doOneThreshold(optimalThreshold), "\n"));
			}
			List<EvaluationMetricValue> predictions = evaluator.computeMetricValues(data, predictionsData.doOneThreshold(optimalThreshold));
			writer.write(String.format("row knn: k=%d, %s, %d\n", kRow, CollectionUtils.join(predictions), milis));
			System.out.println(String.format("row knn: k=%d, %s, %d\n", kRow, CollectionUtils.join(predictions), milis));
			writer.flush();
		}
		writer.close();
	}

	@Test
	public void runFeatureKnn() throws Exception{	
		//RUN
		LCIFData data = new LCIFData();
		data.loadData(trainLibsvm, true);
		data.loadData(testLibsvm, false);
		data.makeIndex();
		//data.printDataStats();
		
		FileWriter writer = new FileWriter("./temp/results-csv-" + data.getDatasetName() + ".csv", true);
		//knn search
		Timer timerS = new Timer("kRow search " + maxK);
		long milisS = -1;

		//feature knn search
		timerS = new Timer("kFeature search " + maxK);
		FeatureKNNCache cacheFeatures = featureKNNSearch.computeAllFeatureLabelSimmilarities(data, maxK2);
		milisS = timerS.end();
		writer.write(String.format("knn-feature-search: %d\n", milisS)); //47 minutes

		//knn feature predictions
		for(int kFeature: kFeatureRange){ //50 sec small K, 10 minutes large k
			Timer timer = new Timer("kFeature " + kFeature);
			LCIFPredictions predictionsData = featureKNNPredictions.makePredictionAllTestRows(data, cacheFeatures, kFeature);
			long milis = timer.end();
			//evaluate
			//featureBasedPredictions.put(kFeature,predictionsData);
			double optimalThreshold = evaluator.computeOptimalSingleThresholdLabelCardinality(data, predictionsData); 
			List<EvaluationMetricValue> predictions = evaluator.computeMetricValues(data, predictionsData.doOneThreshold(optimalThreshold));
			writer.write(String.format("feature knn: k=%d, %s, %d\n", kFeature, CollectionUtils.join(predictions), milis));
			System.out.println(String.format("feature knn: k=%d, %s, %d\n", kFeature, CollectionUtils.join(predictions), milis));
			writer.flush();
			//clear gc
			predictionsData = null;
			System.gc();
		}
		writer.close();
	}
	
	

	@Test
	public void runLCIF() throws Exception{	
		//RUN
		LCIFData data = new LCIFData();
		data.loadData(trainLibsvm, true);
		data.loadData(testLibsvm, false);
		data.makeIndex();
		FileWriter writer = new FileWriter("./temp/results-csv-" + data.getDatasetName() + ".csv", true);
		//run linear combination
		Timer lcif = new Timer("lcif parameter search");
		int run = 0;
		for(int kRow: kRowRange){
			for(int kFeature: kFeatureRange){
				File rowPredictionsCached = new File("./temp/wikiLSHTC_train.txt-predictions-row-knn-k=" + kRow + ".txt");
				File itemPredictionsCached = new File("./temp/wikiLSHTC_train.txt-predictions-feature-knn-k=" + kFeature + ".txt");
				if(!rowPredictionsCached.exists() || !itemPredictionsCached.exists()){
					System.err.println("NO PREDICTIONS FOR rowKnn/FeatureKnn, row=" + kRow + ", feature=" + kFeature);
					continue;
				}
				LCIFPredictions rowPredictions = new LCIFPredictions(); rowPredictions.load(rowPredictionsCached);
				LCIFPredictions featurePredictions =  new LCIFPredictions(); featurePredictions.load(itemPredictionsCached);
				for(double lambda: lambdaRange){
					lcif.progress(run, kRowRange.length * kFeatureRange.length * lambdaRange.length);
					Timer timer = new Timer("kFeature " + kFeature);
					LCIFPredictions predictionsCombo = lcifLinearCombination.makePredictionAllTestRows(data, rowPredictions, featurePredictions, lambda);
					long milis = timer.end();
					//evaluate
					double optimalThreshold = evaluator.computeOptimalSingleThresholdLabelCardinality(data, predictionsCombo); 
					List<EvaluationMetricValue> predictions = evaluator.computeMetricValues(data, predictionsCombo.doOneThreshold(optimalThreshold));
					String str = String.format("lcif knn: kRow=%d, kFeature=%d, lambda=%.3f %s\n", kRow, kFeature, lambda, CollectionUtils.join(predictions), milis);
					System.out.println(str);
					writer.write(str);
					writer.flush();
					run++;
				}
			}
		}
		lcif.end();
		writer.close();
	}
}
