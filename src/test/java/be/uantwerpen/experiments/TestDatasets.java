package be.uantwerpen.experiments;

import java.io.File;

import org.junit.Test;

import be.uantwerpen.lcif_knn.LCIFData;
import be.uantwerpen.util.PreProcessingArffToLibsvm;

public class TestDatasets {

	File datadir = new File("src/main/resources/multi-label-datasets/");
	
	@Test
	public void detailsMedical() throws Exception{	
		File inputTrainArff = new File(datadir,"medical-train.arff");
		File inputTestArff = new File(datadir,"medical-test.arff");
		File labels = new File(datadir,"medical.xml");
		File trainLibsvm = PreProcessingArffToLibsvm.convertArffToLibSVM(inputTrainArff, labels);
		File testLibsvm = PreProcessingArffToLibsvm.convertArffToLibSVM(inputTestArff, labels);
		LCIFData data = new LCIFData();
		data.loadData(trainLibsvm, true);
		data.loadData(testLibsvm, false);
		data.makeIndex();
		System.out.println("medical");
		data.printDataStats();
		//1k
	}
	
	@Test
	public void detailsCorel5k() throws Exception{	
		File inputTrainArff = new File(datadir,"corel5k-train-sparse.arff");
		File inputTestArff = new File(datadir,"corel5k-test-sparse.arff");
		File labels = new File(datadir,"corel5k-sparse.xml");
		File trainLibsvm = PreProcessingArffToLibsvm.convertArffToLibSVM(inputTrainArff, labels);
		File testLibsvm = PreProcessingArffToLibsvm.convertArffToLibSVM(inputTestArff, labels);
		LCIFData data = new LCIFData();
		data.loadData(trainLibsvm, true);
		data.loadData(testLibsvm, false);
		data.makeIndex();
		System.out.println("corel5k-sparse");
		data.printDataStats();
		//5k
	}
	
	@Test
	public void detailsBibtex() throws Exception{	
		File inputTrainArff = new File(datadir,"bibtex-train.arff");
		File inputTestArff = new File(datadir,"bibtex-test.arff");
		File labels = new File(datadir,"bibtex.xml");
		File trainLibsvm = PreProcessingArffToLibsvm.convertArffToLibSVM(inputTrainArff, labels);
		File testLibsvm = PreProcessingArffToLibsvm.convertArffToLibSVM(inputTestArff, labels);
		LCIFData data = new LCIFData();
		data.loadData(trainLibsvm, true);
		data.loadData(testLibsvm, false);
		data.makeIndex();
		System.out.println("bibtex");
		data.printDataStats(); //7k
	}
	
	@Test
	public void detailsDelicious() throws Exception{	
		File inputTrainArff = new File(datadir,"delicious-train.arff");
		File inputTestArff = new File(datadir,"delicious-test.arff");
		File labels = new File(datadir,"delicious.xml");
		File trainLibsvm = PreProcessingArffToLibsvm.convertArffToLibSVM(inputTrainArff, labels);
		File testLibsvm = PreProcessingArffToLibsvm.convertArffToLibSVM(inputTestArff, labels);
		LCIFData data = new LCIFData();
		data.loadData(trainLibsvm, true);
		data.loadData(testLibsvm, false);
		data.makeIndex();
		System.out.println("delicious");
		data.printDataStats();//16k
	}
	
	@Test
	public void detailsImdb() throws Exception{	
		//File arff = new File(datadir,"IMDB-F.arff");
		//MakeXML.main(args);
		//File xml = new File(datadir,"IMDB-F.xml");
		//MakeARFF.split(arff.getAbsolutePath(), xml.getAbsolutePath(), 60.0);	
		File inputTrainArff = new File(datadir,"IMDB-F-train.arff");
		File inputTestArff = new File(datadir,"IMDB-F-test.arff");
		File labels = new File(datadir,"IMDB-F.xml");
		File trainLibsvm = PreProcessingArffToLibsvm.convertArffToLibSVM(inputTrainArff, labels);
		File testLibsvm = PreProcessingArffToLibsvm.convertArffToLibSVM(inputTestArff, labels);
		LCIFData data = new LCIFData();
		data.loadData(trainLibsvm, true);
		data.loadData(testLibsvm, false);
		data.makeIndex();
		System.out.println("imdb");
		data.printDataStats();//120k
	}
	
	
	@Test
	public void detailsExtremeWiki10() throws Exception{	
		File trainLibsvm = new File(datadir, "/extreme/wiki10_train.txt");
		File testLibsvm =  new File(datadir, "/extreme/wiki10_test.txt");
		LCIFData data = new LCIFData();
		data.loadData(trainLibsvm, true);
		data.loadData(testLibsvm, false);
		data.makeIndex();
		System.out.println("wiki-10");
		data.printDataStats(); //20k wel 10000k features
	}
	
	@Test
	//-Xmx10G
	public void detailsExtremeWikiLSHTC() throws Exception{	
		File trainLibsvm = new File(datadir, "/extreme/wikiLSHTC_train.txt");
		File testLibsvm =  new File(datadir, "/extreme/wikiLSHTC_test.txt");
		LCIFData data = new LCIFData();
		data.loadData(trainLibsvm, true);
		data.loadData(testLibsvm, false);
		data.makeIndex(); // 8.9 min for training
		System.out.println("wiki-lshtc");
		data.printDataStats();//2.3M, 1.6M, 325
	}
}
