package be.uantwerpen.lcif_knn;

import java.io.File;
import java.util.List;

import org.junit.Test;

import be.uantwerpen.lcif_knn.LCIFEvaluation.EvaluationMetricValue;
import be.uantwerpen.mulan_wrapper.MulanEvaluatorAdapterForLCIF;

public class TestLCIFEvaluation {

	@Test
	public void compareEvaluationWithMulan() throws Exception{
		File inputTrainArff = new File("src/main/resources/multi-label-datasets/medical-train.arff");
		File inputTestArff = new File("src/main/resources/multi-label-datasets/medical-test.arff");
		File labels = new File("src/main/resources/multi-label-datasets/medical.xml");
		File trainLibsvm  = new File("src/main/resources/multi-label-datasets/medical-train.libsvm");
		File testLibsvm = new File("src/main/resources/multi-label-datasets/medical-test.libsvm");
		
		LCIFData data = new LCIFData(trainLibsvm, testLibsvm);
		RowKNNSearch search = new RowKNNSearch();
		RowKNNCache cache = search.computeAllSimilarRows(data, 20);
		RowKNNPredictions makePredictions = new RowKNNPredictions();
		LCIFPredictions predictions = makePredictions.makePredictionAllTestRows(data, cache, 20);
		LCIFEvaluation evaluator = new LCIFEvaluation();
		evaluator.setUseNoLabelPredictHighesHeuristic();
		double t = evaluator.computeOptimalSingleThresholdLabelCardinality(data, predictions);
		predictions = predictions.doOneThreshold(t);
		
		//evaluate us:
		List<EvaluationMetricValue> metrics = evaluator.computeMetricValues(data, predictions);
		System.out.println(metrics);
		
		//evaluate mulan:
		MulanEvaluatorAdapterForLCIF evaluatorMulan = new MulanEvaluatorAdapterForLCIF();
		evaluatorMulan.evaluate(inputTrainArff, inputTestArff, labels, "row-knn", predictions);
	}
}
