package be.uantwerpen.lcif_knn;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import be.uantwerpen.util.util.IOUtils;

public class TestLCIFLinearCombination {

	File trainFile = new File("./temp/train.libsvm");
	File testFile = new File("./temp/test.libsvm");
	private void makeTestSample() throws IOException{
		String train = 
				"1 1001:1,1002:1, 1003:1\n" +   //1 is label...
				"2 1001:1\n" + 
				"3 1003:1\n";
		IOUtils.saveFile(train, trainFile);
		String test = 
				"1,2 1001:1,1002:1, 1003:1\n" + //prediction 1? = 1 * 1 + 0.57 * 0.0/ (1 + 0.57) = 1/1.57 = 0.63
				"2 1001:1\n" + 					//prediction 1? = 0.57 * 1 + 1.0 * 0 / (1 + 0.57)  = 0.57 / 1.57 = 0.36 .. 
				"3 1003:1\n";
		IOUtils.saveFile(test, testFile);		
	}
	
	@Test
	public void runLinearCombinationRowFeatureKNN() throws IOException{
		makeTestSample();
		//load data
		LCIFData data = new LCIFData();
		data.loadData(trainFile, true);
		data.loadData(testFile, false);
		data.makeIndex();
		data.printDataStats();
		
		//row based predictions
		File neighborFile = new File("./temp/neigbours.txt");
		RowKNNSearch search = new RowKNNSearch();
		search.computeAllSimilarRows(data, 3, neighborFile); //saved to disk
		IOUtils.printHead(neighborFile);
		RowKNNCache cache = new RowKNNCache();
		cache.load(neighborFile);
		File prediction = new File("./temp/predictions-row.txt");
		RowKNNPredictions predictions = new RowKNNPredictions();
		predictions.makePredictionAllTestRows(data, cache, 2, prediction);
		IOUtils.printHead(prediction);
		LCIFPredictions predictionsDataRow = new LCIFPredictions();
		predictionsDataRow.load(prediction);
		
		//feature based predictions
		FeatureKNNSearch searchFeature = new FeatureKNNSearch();
		FeatureKNNCache cacheFeatures = searchFeature.computeAllFeatureLabelSimmilarities(data, 3);
		FeatureKNNPredictions predictionFeatures = new FeatureKNNPredictions();
		File predictionFeaturesFile = new File("./temp/predictions-features.txt");
		predictionFeatures.makePredictionAllTestRows(data, cacheFeatures, 2, predictionFeaturesFile);
		IOUtils.printHead(predictionFeaturesFile);
		LCIFPredictions predictionsDataFeatures = new LCIFPredictions();
		predictionsDataFeatures.load(predictionFeaturesFile);
		
		//combination
		File combination = new File("./temp/predictions-combinated-lambda=0.5.txt");
		LCIFLinearCombination linearCombination = new LCIFLinearCombination();
		linearCombination.makePredictionAllTestRows(data, predictionsDataRow, predictionsDataFeatures, 0.5, combination);
		IOUtils.printHead(combination);
		LCIFPredictions predictionsCombined = new LCIFPredictions();
		predictionsCombined.load(combination);
		
		//combinations-only for testing
		File combinationRowOnly = new File("./temp/predictions-combinated-lambda=1.0.txt");
		linearCombination.makePredictionAllTestRows(data, predictionsDataRow, predictionsDataFeatures, 1.0, combinationRowOnly);
		IOUtils.printHead(combinationRowOnly);
		LCIFPredictions predictionsRowOnly = new LCIFPredictions();
		predictionsRowOnly.load(combinationRowOnly);
		
		File combinationFeatureOnly = new File("./temp/predictions-combinated-lambda=0.0.txt");
		linearCombination.makePredictionAllTestRows(data, predictionsDataRow, predictionsDataFeatures, 0.0, combinationFeatureOnly);
		IOUtils.printHead(combinationFeatureOnly);
		LCIFPredictions predictionsFeaturesOnly = new LCIFPredictions();
		predictionsFeaturesOnly.load(combinationFeatureOnly);

		
		//evaluatie
		LCIFEvaluation evaluator = new LCIFEvaluation();
		System.out.println("row knn");
		evaluator.printBestPredictionAssumingOracle(data, predictionsDataRow);
		System.out.println("combination lambda 1.0 (should be same)");
		evaluator.printBestPredictionAssumingOracle(data, predictionsRowOnly);
		System.out.println("feature knn");
		evaluator.printBestPredictionAssumingOracle(data, predictionsDataFeatures);
		System.out.println("combination lambda 0.0 (should be same)");
		evaluator.printBestPredictionAssumingOracle(data, predictionsFeaturesOnly);
		System.out.println("combination lambda 0.5");
		evaluator.printBestPredictionAssumingOracle(data, predictionsCombined);
	}
}
