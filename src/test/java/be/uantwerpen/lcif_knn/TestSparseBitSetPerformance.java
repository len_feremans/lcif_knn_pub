package be.uantwerpen.lcif_knn;

import java.util.HashSet;

import org.junit.Test;

import be.uantwerpen.util.Timer;
import be.uantwerpen.util.util.MathUtils;

import com.zaxxer.sparsebits.SparseBitSet;

public class TestSparseBitSetPerformance {
	
	long noIterations = 100000;
	long average_size = 1000;
	int max_int = 10000;
	
	@Test
	public void testPerformanceSparseBitSet(){
		Timer timer = new Timer("test performance-sparsebitset");
		for(int iteration=0; iteration<noIterations;iteration++){
			//create 2 sets
			SparseBitSet s =  new SparseBitSet();
			for(int i=0; i<average_size; i++){
				int k = MathUtils.randomInteger(0, max_int);
				s.set(k);
			}
			SparseBitSet s2 =  new SparseBitSet();
			for(int i=0; i<average_size; i++){
				int k = MathUtils.randomInteger(0, max_int);
				s2.set(k);
			}
			s.and(s2);
			//perform intersection
			s.cardinality();
		}
		timer.end();
	}
	
	@Test
	public void testPerformancesHashSet(){
		Timer timer = new Timer("test performance-hash");
		for(int iteration=0; iteration<noIterations;iteration++){
			//create 2 sets
			HashSet<Integer> s =  new HashSet<>();
			for(int i=0; i<average_size; i++){
				int k = MathUtils.randomInteger(0, max_int);
				s.add(k);
			}
			HashSet<Integer> s2 =  new HashSet<>();
			for(int i=0; i<average_size; i++){
				int k = MathUtils.randomInteger(0, max_int);
				s2.add(k);
			}
			//perform intersection
			s.removeAll(s2);
			s.size();
		}
		timer.end();
	}
}
