package be.uantwerpen.lcif_knn;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import be.uantwerpen.util.util.IOUtils;

public class TestFeatureKNNPredictions {

	File trainFile = new File("./temp/train.libsvm");
	File testFile = new File("./temp/test.libsvm");
	private void makeTestSample() throws IOException{
		String train = 
				"1 1001:1,1002:1, 1003:1\n" +   //1 is label...
				"2 1001:1\n" + 
				"3 1003:1\n";
		IOUtils.saveFile(train, trainFile);
		String test = 
				"1,2 1001:1,1002:1, 1003:1\n" + //prediction 1? = 1 * 1 + 0.57 * 0.0/ (1 + 0.57) = 1/1.57 = 0.63
				"2 1001:1\n" + 					//prediction 1? = 0.57 * 1 + 1.0 * 0 / (1 + 0.57)  = 0.57 / 1.57 = 0.36 .. 
				"3 1003:1\n";
		IOUtils.saveFile(test, testFile);		
	}
	
	@Test
	public void runFeatureKnn() throws IOException{
		makeTestSample();
		//load data
		LCIFData data = new LCIFData();
		data.loadData(trainFile, true);
		data.loadData(testFile, false);
		data.makeIndex();
		data.printDataStats();
		
		//make neighbours
		FeatureKNNSearch search = new FeatureKNNSearch();
		FeatureKNNCache cache = search.computeAllFeatureLabelSimmilarities(data, 3);
	
		//make predictions
		File prediction = new File("./temp/predictions-item.txt");  
		FeatureKNNPredictions predictions = new FeatureKNNPredictions();
		predictions.makePredictionAllTestRows(data, cache, 2, prediction);
		IOUtils.printHead(prediction);
		//0,1:0.804737854124,2:0.707106781187,3:0.707106781187,    
										//->  label1 ^ 1001 =1, label1 ^ 1002=1, label1 ^ 1003=1
										//	  = simil(l1,01) + simil(l1,02) + simil(l1,03) 
										//	  = 1/sqrt(2)    + 1/sqrt(1)   + 1/sqrt(2)  ok... zeker, maar toch beetje raar 
										//->  label2 ^ 1001 =3
										//->  label3 ^ 1003 =1
		//1,1:0.707106781187,2:0.707106781187,
		//2,1:0.707106781187,3:0.707106781187,   -> 1003 heeft label1 heeft simil met 1003 (voor user 0)
														// -> simil(1003,label1) = (1 + 0) / (sqrt(1) * sqrt(2)=1.4) = 1/1.4=0.707
														//  ->simil(1003,label3) = (1 + 0) / (sqrt(1) + sqrt(2)=1.4) = 0.707
		//evaluate
		LCIFPredictions predictionsData = new LCIFPredictions();
		predictionsData.load(prediction);
		LCIFEvaluation evaluator = new LCIFEvaluation();
		evaluator.printBestPredictionAssumingOracle(data, predictionsData);
	}
}
