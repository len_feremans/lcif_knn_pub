package be.uantwerpen.lcif_knn;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import be.uantwerpen.util.util.IOUtils;

public class TestRowKNNPredictions {

	File trainFile = new File("./temp/train.libsvm");
	File testFile = new File("./temp/test.libsvm");
	private void makeTestSample() throws IOException{
		String train = 
				"1 1001:1,1002:1, 1003:1\n" +   //1 is label...
				"2 1001:1\n" + 
				"3 1003:1\n";
		IOUtils.saveFile(train, trainFile);
		String test = 
				"1,2 1001:1,1002:1, 1003:1\n" + //prediction 1? = 1 * 1 + 0.57 * 0.0/ (1 + 0.57) = 1/1.57 = 0.63
				"2 1001:1\n" + 					//prediction 1? = 0.57 * 1 + 1.0 * 0 / (1 + 0.57)  = 0.57 / 1.57 = 0.36 .. 
				"3 1003:1\n";
		IOUtils.saveFile(test, testFile);		
	}
	
	@Test
	public void runRowKNN() throws IOException{
		makeTestSample();
		//load data
		LCIFData data = new LCIFData();
		data.loadData(trainFile, true);
		data.loadData(testFile, false);
		data.makeIndex();
		data.printDataStats();
		
		//make neighbours
		File neighborFile = new File("./temp/neigbours.txt");
		RowKNNSearch search = new RowKNNSearch();
		search.computeAllSimilarRows(data, 3, neighborFile); //saved to disk
		IOUtils.printHead(neighborFile);
		RowKNNCache cache = new RowKNNCache();
		cache.load(neighborFile);
		
		//make predictions
		File prediction = new File("./temp/predictions.txt");
		RowKNNPredictions predictions = new RowKNNPredictions();
		predictions.makePredictionAllTestRows(data, cache, 2, prediction);
		IOUtils.printHead(prediction);
		
		//evaluate
		LCIFPredictions predictionsData = new LCIFPredictions();
		predictionsData.load(prediction);
		LCIFEvaluation evaluator = new LCIFEvaluation();
		evaluator.printBestPredictionAssumingOracle(data, predictionsData);
	}
}
