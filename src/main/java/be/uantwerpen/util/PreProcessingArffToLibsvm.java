package be.uantwerpen.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import be.uantwerpen.util.util.CollectionUtils;
import be.uantwerpen.util.util.IOUtils;
import be.uantwerpen.util.util.IOUtils.Streamer;
import be.uantwerpen.util.util.Utils;

public class PreProcessingArffToLibsvm {
	
	public static File convertArffToLibSVM(File arff, File labelsXML) throws Exception{
		//convert arff to libsvm
		List<String> attributes = Utils.getAttributeNamesArff(arff);
		List<String[][]> data  = null;
		if(isSparse(arff)){
			 data = loadSparseDataMatrix(arff);
		}
		else{
			throw new RuntimeException("Data not sparse!");
		}
		List<String> labels = new ArrayList<>();
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		org.w3c.dom.Document document = documentBuilder.parse(labelsXML);
		//	<labels xmlns="http://mulan.sourceforge.net/labels">
		//	<label name="Class-0-593_70"></label>
		NodeList nodes = document.getElementsByTagName("label");
		for(int i=0; i< nodes.getLength(); i++){
			Node node = nodes.item(i);
			String label = node.getAttributes().getNamedItem("name").getTextContent();
			labels.add(label);
		}
		//further process
		List<Set<Integer>> data2 = new ArrayList<>();
		for(String[][] arr: data){
			Set<Integer> row = new HashSet<Integer>();
			for(String[] pair: arr){
				row.add(Integer.valueOf(pair[0]));
			}
			data2.add(row);
		}
		Set<Integer> labelsSet = new HashSet<Integer>();
		for(String label: labels){
			int idx = attributes.indexOf(label);
			labelsSet.add(idx);
			if(idx == -1)
				throw new RuntimeException("label not found " + label);
		}
		//save to libsvm format
		//e.g. labelid1, labelid2, ..., labelidK feature1:val1 feature2:val2 ... featureX:valX
		File output = IOUtils.getFileWithDifferentExtension(arff, "libsvm");
		FileWriter writer = new FileWriter(output);
		for(Set<Integer> row: data2){
			List<Integer> labelsRow =new ArrayList<>();
			List<String> featuresRow = new ArrayList<>();
			for(Integer id: row){
				if(labelsSet.contains(id)){
					labelsRow.add(id);
				}
				else{
					featuresRow.add(id + ":1");
				}
			}
			String outputStr = CollectionUtils.join(labelsRow) + " " + CollectionUtils.join(featuresRow, " ") + "\n";
			writer.write(outputStr);
		}
		writer.close();
		System.out.println("Saved " + output.getAbsolutePath());
		return output;
	}

	private static boolean isSparse(File input) {
		final Boolean[] sparse = new Boolean[]{null};
		try{
		IOUtils.stream(input, new Streamer() {
			
			boolean isData = false;
			@Override
			public void doLine(String line) {
				if(line.startsWith("@data"))
				{
					isData = true;
					return;
				}
				if(isData){
					if(line.isEmpty())
						return;
					if(line.startsWith("{"))
						sparse[0] = true;
					else
						sparse[0] = false;
					throw new RuntimeException("escaping streamer");
				}
			}
		});
		}catch(Exception e){
			return sparse[0];
		}
		throw new RuntimeException("should not get here");
	}
	
	private static List<String[][]> loadSparseDataMatrix(File arffFile) throws IOException
	{
		final long count = IOUtils.countLines(arffFile);
		Timer timer = null;
		if(count > 50000)
			timer = new Timer("loadSparseDataMatrix " + arffFile.getName());
		final Timer timer2 = timer;
		final List<String[][]> output = new ArrayList<String[][]>();
		IOUtils.stream(arffFile, new Streamer() {
			
			private int i=0;
			private boolean data = false;
			@Override
			public void doLine(String line) {
				i++;
				if(i % 500000 == 0)
					timer2.progress("Loading",i,count);
				if(!data && line.startsWith("@data")){
					data = true;
				}
				else if(data)
					output.add(getSparseRow(line));
			}
		});
		if(count > 50000)
			timer.end();
		return output;
	}
	
	private static Pattern split_comma = Pattern.compile("\\s*,\\s*");
	private static Pattern split_whitespace = Pattern.compile("\\s+");
	
	private static String[][] getSparseRow(String row)
	{
		row = Utils.substringBetween(row, "{","}");
		if(row.trim().isEmpty())
			return new String[0][];
		String[] pairs = split_comma.split(row);
		String[][] pairsSplitted = new String[pairs.length][];
		for(int n=0; n< pairs.length; n++)
		{
			String[] pair = split_whitespace.split(pairs[n]);
			//pair[1] = prePreprocessArffValue(pair[1]);
			pairsSplitted[n] = pair;
		}
		return pairsSplitted;
	}
	
}
