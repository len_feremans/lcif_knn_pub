package be.uantwerpen.util.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

import be.uantwerpen.util.util.CollectionUtils;

public class MyMinHeap<M extends Comparable, V> {

	private PriorityQueue<Pair<M,V>> pQueue;
	private int maxSize = 0;
	
	
	private Comparator<Pair<M,V>> lowToHigh = new Comparator<Pair<M,V>>() {

		@Override
		public int compare(Pair<M, V> o1, Pair<M, V> o2) {
			return o1.getFirst().compareTo(o2.getFirst());
		}
	};
	
	private Comparator<Pair<M,V>> highToLow = new Comparator<Pair<M,V>>() {

		@Override
		public int compare(Pair<M, V> o1, Pair<M, V> o2) {
			return o2.getFirst().compareTo(o1.getFirst());
		}
	};
	
	public MyMinHeap(int maxSize){
		this.maxSize = maxSize;
		pQueue = new PriorityQueue<Pair<M,V>>(maxSize, lowToHigh);
	}
	
	public void clear(){
		pQueue.clear();
	}
	
	public long size(){
		return pQueue.size();
	}
	
	@SuppressWarnings("unchecked")
	public boolean add(M value, V element){
		if(pQueue.size() >= maxSize){
			Pair<M,V> lowest = pQueue.peek();
			if(lowest.getFirst().compareTo(value) < 0){
				pQueue.remove();
				pQueue.add(new Pair<M,V>(value,element));
				return true;
			}
			else{
				return false;
			}
		}
		else{
			pQueue.add(new Pair<M,V>(value,element));
			return true;
		}
	}
	
	public List<Pair<M,V>> values(){
		List<Pair<M,V>> values = new ArrayList<>(pQueue.size());
		for(Pair<M,V> entry: pQueue){
			values.add(entry); //note: not in order
		}
		Collections.sort(values,highToLow);
		return values;
	}
	
	//test
	public static void main(String[] args){
		MyMinHeap<Double, String> heap = new MyMinHeap<>(3);
		heap.add(1.0, "A");
		heap.add(1.2, "B");
		heap.add(0.8, "C");
		heap.add(0.7, "D");
		System.out.println(CollectionUtils.join(heap.values()));
		heap.add(0.9, "C+");
		System.out.println(CollectionUtils.join(heap.values()));
		heap.add(2.0, "A+");
		System.out.println(CollectionUtils.join(heap.values()));
		heap.add(1.01, "B+");
		System.out.println(CollectionUtils.join(heap.values()));
	}
}
