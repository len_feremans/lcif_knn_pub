package be.uantwerpen.util.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Utils {
	
	public static List<String> getAttributeNamesArff(File file) throws IOException{
		List<String> lines = IOUtils.readFileUntil(file,"@data");
		List<String> attributeNames = new ArrayList<String>();
		//fix header
		for(String line: lines){
			if(line.startsWith("@attribute")){
				String name = line.split("\\s+")[1];
				attributeNames.add(name);
			}
		}
		return attributeNames;
	}
	
	public static String milisToStringReadable(long milis){
		if(milis < 1000){
			return String.format("%d ms", milis);
		}
		else if(milis >= 1000 && milis < 60000){
			return String.format("%.1f sec", milis/1000.0);
		}
		else if(milis >= 60 * 1000 && milis < 60 * 60 * 1000){
			return String.format("%.1f min", milis/(60 * 1000.0));
		}
		//if(milis >= 360000){
		return String.format("%.2f h", milis/(3600 * 1000.0));		
	}
	
	public static String substringBefore(String str, String suffix)
	{
		int idx = str.indexOf(suffix);
		if(idx != -1)
			return str.substring(0, idx);
		else
			return "";
	}
	
	public static String substringAfter(String str, String prefix)
	{
		int idx = str.indexOf(prefix);
		if(idx != -1)
			return str.substring(idx + prefix.length());
		else
			return "";
	}
	
	public static String substringBetween(String str, String prefix, String suffix)
	{
		return Utils.substringBefore(Utils.substringAfter(str, prefix), suffix);
	}
}
