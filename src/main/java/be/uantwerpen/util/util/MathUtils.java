package be.uantwerpen.util.util;

import java.util.Collection;
import java.util.Random;

public class MathUtils {
	
	private static Random rand = new Random(1);
	
	public static int randomInteger(int min, int maxInclusive){
	    int randomNum = rand.nextInt(maxInclusive - min + 1) + min;
	    return randomNum;
	}
	
	public static double[] makeRange(double min, double maxExclusive, double delta){
		int size = (int) Math.floor((maxExclusive-min)/delta);
		double[] range = new double[size];
		int i=0;
		for(double d = min; i<size; d+=delta){
			range[i++] = d;
		}
		return range;
	}
	
	public static double[] makeRangeIncl(double min, double maxInclusive, double delta){
		int size = (int) Math.floor((maxInclusive-min)/delta);
		double[] range = new double[size+1];
		int i=0;
		for(double d = min; i<size; d+=delta){
			range[i++] = d;
		}
		range[i++] = maxInclusive;
		return range;
	}
	
	
	public static int[] makeRange(int min, int maxExclusive, int delta){
		int size = (int) Math.floor((maxExclusive-min)/delta);
		int[] range = new int[size];
		int i=0;
		for(int d = min; i<size; d+=delta){
			range[i++] = d;
		}
		return range;
	}
	
	public static int minInt(Collection<Integer> x)
	{
		if(x.size() == 0)
			return Integer.MIN_VALUE;
	    int min = x.iterator().next();
		for(int i: x)
			min = Math.min(min, i);
		return min;
	}
	
	public static int maxInt(Collection<Integer> ints) {
		int max = ints.iterator().next();
		for(int i: ints)
			max = Math.max(max, i);
		return max;
	}
	
	public static int maxInt(int ... ints) {
		int max = ints[0];
		for(int i: ints)
			max = Math.max(max, i);
		return max;
	}
	

}
