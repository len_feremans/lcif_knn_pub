package be.uantwerpen.util.util;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Generic utilities for Collections and Arrays
 * 
 * @author lfereman
 *
 */
public class CollectionUtils {


	public static <T extends Collection<?>> String join(T collection, String seperator) {
		return join(collection, seperator, "%s");
	}
	
	private static <T extends Collection<?>> String join(T collection, String seperator, String formatter) {
		StringBuffer buffer = new StringBuffer();
		int idx =0;
		for(Object el: collection)
		{
			buffer.append(String.format(formatter,el));
			if(idx != collection.size() -1)
				buffer.append(seperator);
			idx++;
		}
		return buffer.toString();
	}


	public static <T extends Collection<?>> String join(T collection) {
		return join(collection, ", ");
	}

	public static <K,V, T extends Map<K,V>> String join(Map<K,V> collection, String keyValueSeperator, String entrySeperator) {
		if(collection == null)
			return "null";
		StringBuffer buffer = new StringBuffer();
		int idx =0;
		for(Entry<K,V> entr: collection.entrySet())
		{
			buffer.append(entr.getKey().toString());
			buffer.append(keyValueSeperator);
			buffer.append(entr.getValue().toString());
			if(idx != collection.size() -1)
				buffer.append(entrySeperator);
			idx++;
		}
		return buffer.toString();
	}

}
