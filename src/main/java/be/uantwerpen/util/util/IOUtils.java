package be.uantwerpen.util.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import be.uantwerpen.util.model.Pair;

/**
 * Generic utils for:
 *   Saving/loading text files.
 *   Finding string in files
 *   Filename manipulation
 *   
 * @author lfereman
 *
 */ 
public class IOUtils {
	
	public static List<Pair<File,File>> randomFolds(File textfile, int folds) throws Exception{
		List<String> data = IOUtils.readFile(textfile);
		Random seed = new Random(0);
		Collections.shuffle(data, seed); //fix seed
		List<Pair<File,File>> pairs = new ArrayList<>();
		int testSize = data.size()/folds;	
		for(int i=0; i<folds; i++){
			int startTest = i * testSize;
			int endTest = startTest + testSize;
			List<String> contentTraining = new ArrayList<String>();
			contentTraining.addAll(data.subList(0, startTest));
			contentTraining.addAll(data.subList(endTest, data.size()));
			List<String> contentTest = new ArrayList<String>();
			contentTest.addAll(data.subList(startTest, endTest));
			File train = IOUtils.getTempFileWithDifferentExtension(textfile, "train-fold-" + i + ".libsvm");
			File test = IOUtils.getTempFileWithDifferentExtension(textfile, "test-fold-" + i + ".libsvm");
			IOUtils.saveFile(contentTraining ,train);
			IOUtils.saveFile(contentTest, test);
			pairs.add(new Pair<>(train,test));
		}
		return pairs;
	}
	
	public static void saveFile(List<String> content, File file) throws Exception
	{
		if(!file.getParentFile().exists())
			file.getParentFile().mkdirs();
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		int idx = 0;
		for(String line: content)
		{
			writer.write(line);
			if(idx != content.size()-1)
				writer.write("\n");
			idx++;
		}
		writer.flush();
		writer.close();
		System.out.format("Saved %s\n", file.getName());
	}

	public static void saveFile(String s, File file) throws IOException
	{
		saveFile(s, file, Encoding.DEFAULT);
	}
	
	public static void saveFile(String s, File file, Encoding encoding) throws IOException
	{
		if(!file.getParentFile().exists())
			file.getParentFile().mkdirs();
		BufferedWriter writer = null;
		if(encoding.equals(Encoding.DEFAULT))
			 writer = new BufferedWriter(new FileWriter(file));
		else if(encoding.equals(Encoding.UTF_8))
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
		else
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "ISO-8859-1"));
		writer.write(s);
		writer.flush();
		writer.close();
		System.out.format("Saved %s\n", file.getName());
	}
	
	public static File getTempFileWithDifferentExtension(File file, String ext) 
	{
		return new File("./temp", getFilenameNoExtension(file) + "." + ext);
	}
	
	
	public static String getFilenameNoExtension(File file) 
	{
		int idx = file.getName().lastIndexOf(".");
		if(idx == -1)
			return file.getName();
		else 
			return file.getName().substring(0, idx);
	}

	public static File getFileWithDifferentSuffix(File file, String suffix)
	{
		return new File(file.getParentFile(), getFilenameNoExtension(file) + suffix + "." + getExtension(file));
	}
	
	public static File getFileWithDifferentSuffix(File file, String suffix, String extension)
	{
		return new File(file.getParentFile(), getFilenameNoExtension(file) + suffix + "." + extension);
	}
	
	public static File getFileWithDifferentExtension(File file, String ext) 
	{
		return new File(file.getParentFile(), getFilenameNoExtension(file) + "." + ext);
	}
	
	public static String getExtension(File file) 
	{
		int idx = file.getName().lastIndexOf(".");
		if(idx == -1)
			return "";
		else 
			return file.getName().substring(idx+1);
	}
	
	public static void printHead(File file) throws IOException{	
		printHead(file,10);
	}
	
	public static void printHead(File file, int n) throws IOException{	
		System.out.println("====== HEAD " + file.getName() + " ========");
		List<String> lines = readFileUntil(file, n);
		for(String line: lines)
			System.out.println(line);
	}
	
	private static List<String> readFileUntil(File file, int lineNumber) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		List<String> lines = new ArrayList<String>();
		String current = reader.readLine();
		int line = 1;
		while(current != null)
		{
			lines.add(current);
			current = reader.readLine(); 
			line++;
			if(line >= lineNumber)
				break;
		}
		reader.close();
		return lines;
	}
	
	public static List<String> readFileUntil(File file, String stopOnPrefix) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		List<String> lines = new ArrayList<String>();
		String current = reader.readLine();
		while(current != null)
		{
			lines.add(current);
			current = reader.readLine(); 
			if(current.startsWith(stopOnPrefix))
				break;
		}
		reader.close();
		return lines;
	}
	
	public static long countLines(File input) throws IOException {
		//see http://stackoverflow.com/questions/453018/number-of-lines-in-a-file-in-java
        LineNumberReader  lnr = new LineNumberReader(new FileReader(input));
        lnr.skip(Long.MAX_VALUE);
        long lines = lnr.getLineNumber();
        // Finally, the LineNumberReader object should be closed to prevent resource leak
        lnr.close();
        return lines + 1; //+1 right?
	}
	
	public static File copyPartOfFile(File file, int from, int to) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		File output = new File(file.getParentFile(), file.getName() + "_part_" + from + "_" + to + ".csv");
		FileWriter writer = new FileWriter(output);
		String current = reader.readLine();
		int line = 1;
		while(current != null)
		{
			if(line >= from){
				writer.write(current);
				writer.write("\n");
			}			
			if(line > to){
				break;
			}
			current = reader.readLine(); 
			line++;
		}
		reader.close();
		writer.close();
		System.out.println("Saved " + output);
		return output;
	}

	public enum Encoding{
		DEFAULT,
		UTF_8,
		ISO_8859_1
	}
	
	public static List<String> readFile(File file) throws IOException{
		return readFile(file, Encoding.DEFAULT, -1);
	}
	
	
	private static List<String> readFile(File file, Encoding encoding, int maxLines) throws IOException
	{
		BufferedReader reader = null;
		if(encoding == Encoding.UTF_8)
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		else if(encoding == Encoding.ISO_8859_1)
		{
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "ISO-8859-1"));
		}
		else
			reader = new BufferedReader(new FileReader(file));
		List<String> lines = new ArrayList<String>();
		int idx =0;
		String current = reader.readLine();
		idx++;
		while(current != null)
		{
			lines.add(current);
			if(idx >= maxLines && maxLines !=-1)
				break;
			current = reader.readLine(); 
			idx++;
		}
		reader.close();
		return lines;
	}
	
	public interface Streamer{
		public void doLine(String line);
	}
	
	public static void stream(File input, Streamer streamer) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(input));
		String current = reader.readLine();
		while(current != null)
		{
			streamer.doLine(current);
			current = reader.readLine(); 
		}
		reader.close();
	}
	
	public interface Streamer2{
		public String doLine(String line);
	}
	
	public static void stream2(File input, File output, Streamer2 streamer2, boolean append) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(input));
		BufferedWriter writer = new BufferedWriter(new FileWriter(output, append));
		String current = reader.readLine();
		while(current != null)
		{
			String out = streamer2.doLine(current);
			if(out!=null){
				writer.write(out);
				writer.newLine();
			}
			current = reader.readLine(); 
		}
		reader.close();
		writer.close();
	}

}
