package be.uantwerpen.mulan_wrapper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import mulan.data.MultiLabelInstances;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.RemovePercentage;
import be.uantwerpen.util.model.Pair;
import be.uantwerpen.util.util.IOUtils;
import be.uantwerpen.util.util.IOUtils.Streamer;

public class PreprocessingMakeARFF {

	public static void main(String[] args) throws Exception{
		String dir = "src/main/resources/multi-label-datasets/";
		File input = new File(dir + "/lshtc/train.csv");
		int end = 2000000;
		convert(end);
		File output = IOUtils.getFileWithDifferentSuffix(input,"-" + end, "arff");
		File xml = IOUtils.getFileWithDifferentExtension(output, "xml");
		split(output.getAbsolutePath(), xml.getAbsolutePath(), 60.0);
	}

	
	public static void convert(int end) throws IOException{
		String dir = "src/main/resources/multi-label-datasets/";
		File input = new File(dir + "/lshtc/train.csv");
		Pattern p = Pattern.compile("(\\s)+|(\\s)*(\\,)(\\s)*");
		int start = 1;
		Set<String> allLabels = new HashSet<String>();
		Set<String> allFeatures = new HashSet<String>();
		List<Pair<List<String>, List<String>>> data = new ArrayList<>();
		IOUtils.stream(input, new Streamer() {

			int lineNo = 0;
			@Override
			public void doLine(String line) {
				if(lineNo > end && end != -1){
					lineNo++;
					return;
				}
				if(lineNo < start && end != -1){
					lineNo++;
					return;
				}
				String[] tokens = p.split(line);
				List<String> labels = new ArrayList<String>();
				List<String> features = new ArrayList<String>();
				for(String token: tokens){
					if(token.contains(":")){
						features.add(token.split(":")[0]);
					}
					else{
						labels.add(token);
					}
				}
				lineNo++;
				//add
				allLabels.addAll(labels);
				allFeatures.addAll(features);
				data.add(new Pair<>(labels,features));
			}});
		//save sparse ARFF
		List<String> labels = allLabels.stream().mapToInt(s -> Integer.valueOf(s)).sorted().mapToObj(i -> "L_" + i).collect(Collectors.toList());
		List<String> features = allFeatures.stream().mapToInt(s -> Integer.valueOf(s)).sorted().mapToObj(s -> "F_" + s).collect(Collectors.toList());
		labels.addAll(features);
		Map<String, Integer> map = new HashMap<String,Integer>();
		IntStream.range(0, labels.size()).forEach(i -> map.put(labels.get(i),i));
		List<String> types = new ArrayList<String>();
		for(int i=0; i< labels.size(); i++){
			types.add("{0,1}");
		}
		FileWriter writer = new FileWriter(IOUtils.getFileWithDifferentSuffix(input,"-" + end, "arff"));
		List<String> header = makeHeader("lshtc", labels, types, false);
		for(String h: header){
			writer.write(h); writer.write("\n");
		}
		for(Pair<List<String>,List<String>> pair: data){
			writer.write("{");
			List<Integer> labelsInt = pair.getFirst().stream().map(s -> map.get("L_" + s)).sorted().collect(Collectors.toList());
			for(Integer labelInt: labelsInt){
				writer.write("" + labelInt);
				writer.write(" 1,");
			}
			List<Integer> featuresInt = pair.getSecond().stream().map(s -> map.get("F_" + s)).sorted().collect(Collectors.toList());
			for(Integer featureInt: featuresInt){
				writer.write("" + featureInt);
				writer.write(" 1,");
			}
			writer.write("}\n");
		}
		writer.close();
	}
	

	public static void split(String inputArff, String inputXml, double trainingPercentage) throws Exception{
		if(trainingPercentage < 1.0)
			throw new RuntimeException("Percentage < 1%");
		System.out.println("Loading the dataset");
        MultiLabelInstances mlDataSet = new MultiLabelInstances(inputArff, inputXml);
        // split the data set into train and test
        Instances dataSet = mlDataSet.getDataSet();
        RemovePercentage rmvp = new RemovePercentage();
        rmvp.setInvertSelection(true);
        rmvp.setPercentage(trainingPercentage);
        rmvp.setInputFormat(dataSet);
        Instances trainDataSet = Filter.useFilter(dataSet, rmvp);

        rmvp = new RemovePercentage();
        rmvp.setPercentage(trainingPercentage);
        rmvp.setInputFormat(dataSet);
        Instances testDataSet = Filter.useFilter(dataSet, rmvp);

        ArffSaver saver = new ArffSaver();
        saver.setInstances(trainDataSet);
        saver.setFile(IOUtils.getFileWithDifferentSuffix(new File(inputArff), "-train"));
        saver.writeBatch();
        saver = new ArffSaver();
        saver.setInstances(testDataSet);
        saver.setFile(IOUtils.getFileWithDifferentSuffix(new File(inputArff), "-test"));
        saver.writeBatch();
	}
	
	public static List<String> makeHeader(String relationName, List<String> attributeNames, List<String> attributeTypes, boolean escapeName){
		List<String> lines = new ArrayList<String>();
		lines.add(String.format("@relation %s", relationName));
		for(int i=0; i< attributeNames.size(); i++){
			lines.add(String.format("@attribute %s %s", escapeName?attributeNames.get(i):attributeNames.get(i), attributeTypes.get(i)));
		}
		lines.add("\n@data\n");
		return lines;
	}
}
