package be.uantwerpen.mulan_wrapper;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

import be.uantwerpen.util.util.IOUtils;
import be.uantwerpen.util.util.Utils;

public class PreProcessingMakeXML {

	public static void main(String[] args) throws Exception{
		//String file = "Corel5k-sparse.arff";
		//String file = "lshtc/train-2000000.arff";
		String file = "IMDB-F.arff";
		String dir = "src/main/resources/multi-label-datasets/";
		FileWriter writer = new FileWriter(IOUtils.getFileWithDifferentExtension(new File(dir + file), "xml"));
		writer.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
		writer.write("<labels xmlns=\"http://mulan.sourceforge.net/labels\">\n");
		List<String> attributeNames = Utils.getAttributeNamesArff(new File(dir + file));
		for(String attribute: attributeNames){
			//corel
			//if(!attribute.contains("Cluster")){
			//	writer.write(String.format("<label name=\"%s\"></label>\n", attribute));
			//}
			//lshtc
			//if(attribute.startsWith("L_")){
			//	writer.write(String.format("<label name=\"%s\"></label>\n", attribute));
			//}
			//imdb
			if(attribute.equals("Biography")){
				writer.write(String.format("<label name=\"%s\"></label>\n", attribute));
				break;
			}
			else{
				writer.write(String.format("<label name=\"%s\"></label>\n", attribute));
			}
		}
		writer.write("</labels>\n");
		writer.close();
	}
}
