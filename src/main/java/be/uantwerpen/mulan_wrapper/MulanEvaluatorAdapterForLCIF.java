package be.uantwerpen.mulan_wrapper;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import mulan.classifier.MultiLabelOutput;
import mulan.data.MultiLabelInstances;
import mulan.evaluation.Evaluation;
import mulan.evaluation.GroundTruth;
import mulan.evaluation.measure.ExampleBasedAccuracy;
import mulan.evaluation.measure.ExampleBasedFMeasure;
import mulan.evaluation.measure.HammingLoss;
import mulan.evaluation.measure.MacroFMeasure;
import mulan.evaluation.measure.Measure;
import mulan.evaluation.measure.MicroFMeasure;

import org.apache.commons.lang3.ArrayUtils;

import weka.core.Instance;
import weka.core.Instances;
import be.uantwerpen.lcif_knn.LCIFPredictions;
import be.uantwerpen.lcif_knn.Prediction;
import be.uantwerpen.util.model.MultiKeyMap2;
import be.uantwerpen.util.util.Utils;

/** Why: Validate we produce the same evaluation matric scoresas Mulan?
 * 
 * @author lfereman
 *
 */
public class MulanEvaluatorAdapterForLCIF {

	String[] labelNamesIndexedByMulan = null;
	List<String> labelAndFeatureNamesIndexedByLCIF = null;
			
	public void evaluate(File trainArff, File testArff, File xml, String labelLCIFLearner, LCIFPredictions predictions) throws Exception{
		//MultiLabelInstances datasetTrain =  new MultiLabelInstances(trainArff.getAbsolutePath(), xml.getAbsolutePath());
		MultiLabelInstances mlTestData =  new MultiLabelInstances(testArff.getAbsolutePath(), xml.getAbsolutePath());
		int numLabels = mlTestData.getNumLabels(); //IMPORTANT!
		List<Measure> measures = Arrays.asList(
				new MacroFMeasure(numLabels),
				new MicroFMeasure(numLabels), 
				new ExampleBasedFMeasure(),
				new ExampleBasedAccuracy(),
				new HammingLoss());
		//initialize some stuff
		labelNamesIndexedByMulan = mlTestData.getLabelNames();
		labelAndFeatureNamesIndexedByLCIF =  Utils.getAttributeNamesArff(testArff); //or training is the same
		
		//evaluator.evaluate(learner, datasetTest, measures);
		//based on Evaluator.evaluate(MultiLabelLearner, dataset, measures) but no depedency on the critical
		//MultiLabelOutput output = learner.makePrediction(labelsMissing) function 
		for (Measure m : measures) {
			m.reset();
		}
		int[] labelIndices = mlTestData.getLabelIndices();
		GroundTruth truth;
		Set<Measure> failed = new HashSet<Measure>();
		Instances testData = mlTestData.getDataSet();
		int numInstances = testData.numInstances();
		for (int instanceIndex = 0; instanceIndex < numInstances; instanceIndex++) {
			Instance instance = testData.instance(instanceIndex);
			boolean hasMissingLabels = mlTestData.hasMissingLabels(instance);
			Instance labelsMissing = (Instance) instance.copy();
			labelsMissing.setDataset(instance.dataset());
			for (int i = 0; i < mlTestData.getNumLabels(); i++) {
				labelsMissing.setMissing(labelIndices[i]);
			}
			//Implementation of
			//MultiLabelOutput output = learner.makePrediction(labelsMissing);
			double[] confidences = new double[numLabels]; //default is 0.0
		    boolean[] predictionsRelevant = new boolean[numLabels]; //default is false
		    List<Prediction> predictionsLCIF = predictions.getPredictionsRaw(instanceIndex);
		    for(Prediction pred: predictionsLCIF){
		    	int labelIndex = labelIndexLCIFToLabelIndexMulan(pred.getLabelId());
		    	double score = pred.getScore();
		    	confidences[labelIndex] = score;
		    	if(!Double.isNaN(predictions.getThreshold()) && score > predictions.getThreshold()){
		    		predictionsRelevant[labelIndex] = true;
		    	}
		    	else if(Double.isNaN(predictions.getThreshold())){
		    		predictionsRelevant[labelIndex] = true;
		    	}
		    }
		    
			MultiLabelOutput output = new MultiLabelOutput(predictionsRelevant,confidences);
			if (output.hasPvalues()) {// check if we have regression outputs
				truth = new GroundTruth(getTrueScores(instance, numLabels, labelIndices));
			} else {
				truth = new GroundTruth(getTrueLabels(instance, numLabels, labelIndices));
			}
			Iterator<Measure> it = measures.iterator();
			while (it.hasNext()) {
				Measure m = it.next();
				if (!failed.contains(m)) {
					try {
						if (hasMissingLabels && !m.handlesMissingValues()) {
							continue;
						}
						m.update(output, truth);
					} catch (Exception ex) {
						failed.add(m);
					}
				}
			}
		}
		Evaluation results = new Evaluation(measures, mlTestData);
		//print
		MultiKeyMap2<String, String, String> all_results = new MultiKeyMap2<>();
		Set<String> measureNames = measures.stream().map((m) -> m.getName()).collect(Collectors.toSet());
		for (Measure m : results.getMeasures()) {
			String measureName = m.getName();
			if(!measureNames.contains(measureName))
				continue;
			String value = String.format("%.3f", m.getValue());
			all_results.put(labelLCIFLearner, measureName, value);
		}
		System.out.println(all_results);
	}
	
	//order of label indexes, see MultiLabelInstances.getLabelIndices()
    //our index == attribute-index  (not start with 0)
	private int labelIndexLCIFToLabelIndexMulan(int idx) throws IOException{
		String label = this.labelAndFeatureNamesIndexedByLCIF.get(idx);
		return ArrayUtils.indexOf(this.labelNamesIndexedByMulan, label);
	}

	//copy paste Evaluator: was private...
	private boolean[] getTrueLabels(Instance instance, int numLabels, int[] labelIndices) {

		boolean[] trueLabels = new boolean[numLabels];
		for (int counter = 0; counter < numLabels; counter++) {
			int classIdx = labelIndices[counter];
			String classValue = instance.attribute(classIdx).value((int) instance.value(classIdx));
			trueLabels[counter] = classValue.equals("1");
		}

		return trueLabels;
	}

	private double[] getTrueScores(Instance instance, int numLabels, int[] labelIndices) {

		double[] trueScores = new double[numLabels];
		for (int counter = 0; counter < numLabels; counter++) {
			int classIdx = labelIndices[counter];
			double score;
			if (instance.isMissing(classIdx)) {// if target is missing
				score = Double.NaN; // make it equal to Double.Nan
			} else {
				score = instance.value(classIdx);
			} 
			trueScores[counter] = score;
		}

		return trueScores;
	}
}
