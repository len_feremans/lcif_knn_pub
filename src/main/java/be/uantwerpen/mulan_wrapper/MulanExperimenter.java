package be.uantwerpen.mulan_wrapper;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import mulan.classifier.MultiLabelLearner;
import mulan.classifier.lazy.IBLR_ML;
import mulan.classifier.lazy.MLkNN;
import mulan.classifier.meta.thresholding.OneThreshold;
import mulan.classifier.transformation.BinaryRelevance;
import mulan.data.MultiLabelInstances;
import mulan.evaluation.Evaluation;
import mulan.evaluation.Evaluator;
import mulan.evaluation.MultipleEvaluation;
import mulan.evaluation.measure.ExampleBasedAccuracy;
import mulan.evaluation.measure.ExampleBasedFMeasure;
import mulan.evaluation.measure.HammingLoss;
import mulan.evaluation.measure.MacroFMeasure;
import mulan.evaluation.measure.Measure;
import mulan.evaluation.measure.MicroFMeasure;
import weka.classifiers.functions.SMO;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.model.MultiKeyMap2;
import be.uantwerpen.util.model.Pair;
import be.uantwerpen.util.util.IOUtils;
import be.uantwerpen.util.util.IOUtils.Streamer2;
import be.uantwerpen.util.util.Utils;

public class MulanExperimenter {

	//returns best parameters for each learner
	public MultiKeyMap2<String, String, String> runGridSearch(File train, File test, File xml, int[] kRowRange, int folds) throws Exception{
		MultiLabelInstances dataset =  new MultiLabelInstances(train.getAbsolutePath(), xml.getAbsolutePath());
		List<Pair<MultiLabelLearner,String>> learners = getLearners(dataset, kRowRange);
		int sampleSize = dataset.getNumInstances();
		File sample = takeSample(train, sampleSize);
		MultiLabelInstances sampleDataset =  new MultiLabelInstances(sample.getAbsolutePath(), xml.getAbsolutePath());
		Timer timer = new Timer("Started grid search");
		MultiKeyMap2<String, String, String> results = runInnerLearners(learners, sampleDataset, folds);
		timer.end();
		return results;
	}
	
	public void reportParameters(MultiKeyMap2<String, String, String> gridsearchResuls, File outputBestGS) throws IOException{
		FileWriter writer = new FileWriter(outputBestGS);
	    // ,Macro-averaged F-Measure      ,Micro-averaged F-Measure      ,threshold                     ,time milis                    ,
	    // br_smo                        ,0.7446 (+-0.0487)             ,0.7703 (+-0.0344)             ,0.000000                      ,46784                         ,
	    // iblr_ml k=1                   ,0.5571 (+-0.0327)             ,0.4939 (+-0.0781)             ,0.000000                      ,15340                         ,
	    // iblr_ml k=11                  ,0.3996 (+-0.0411)             ,0.4560 (+-0.0514)             ,0.000000                      ,25804                         ,
		// ml-knn k=9                    ,0.6425 (+-0.0445)             ,0.6101 (+-0.0628)             ,0.000000                      ,1926                          ,
		//double tBR_SMO =  Double.parseDouble(gridsearchResuls.get("br_smo", "threshold"));
		//writer.write(String.format("br_smo;t=%.3f\n",tBR_SMO)); 
		//best threshold for iblr_m;
		List<String> iblrResults = gridsearchResuls.keySet().stream().filter((key) -> key.contains("iblr_ml")).collect(Collectors.toList());
		for(String metric: new String[]{"Macro-averaged F-Measure", "Micro-averaged F-Measure", "Example-Based Accuracy", "Example-Based F Measure","Hamming Loss"}){
			String optimalParameter = bestResult(iblrResults, gridsearchResuls, metric);
			String outputStr = String.format("method=iblr-knn;mode=gridsearch;%s;t=%s;%s=%s\n", 
					Utils.substringAfter(optimalParameter, "iblr_ml "), 
					0.0, //gridsearchResuls.get(optimalParameter, "threshold"),
					metric, gridsearchResuls.get(optimalParameter, metric));
			System.out.print(outputStr);
			writer.write(outputStr);
		}
		//best threshold for ml-knn;
		List<String> mlknnResults = gridsearchResuls.keySet().stream().filter((key) -> key.contains("ml-knn")).collect(Collectors.toList());
		for(String metric: new String[]{"Macro-averaged F-Measure", "Micro-averaged F-Measure", "Example-Based Accuracy", "Example-Based F Measure","Hamming Loss"}){
			String optimalParameter = bestResult(mlknnResults, gridsearchResuls, metric);
			String outputStr = String.format("method=ml-knn;mode=gridsearch;%s;t=%s;%s=%s\n", 
					Utils.substringAfter(optimalParameter, "ml-knn "), 
					0.0, //gridsearchResuls.get(optimalParameter, "threshold"),
					metric, gridsearchResuls.get(optimalParameter, metric));
			System.out.print(outputStr);
			writer.write(outputStr);
		}
		writer.close();
	}
	
	public List<Pair<MultiLabelLearner, String>> parseLearners(MultiLabelInstances dataset, File outputBestGS) throws Exception{
		/* Parse:
		 * br_smo;t=0.000
method=iblr-knn;mode=gridsearch;k=1;t=0.000000;Macro-averaged F-Measure=0.688
method=iblr-knn;mode=gridsearch;k=1;t=0.000000;Micro-averaged F-Measure=0.508
method=iblr-knn;mode=gridsearch;k=11;t=0.000000;Example-Based Accuracy=0.431
method=iblr-knn;mode=gridsearch;k=11;t=0.000000;Example-Based F Measure=0.485
method=iblr-knn;mode=gridsearch;k=21;t=0.000000;Hamming Loss=0.037
method=ml-knn;mode=gridsearch;k=21;t=0.000000;Macro-averaged F-Measure=0.752
method=ml-knn;mode=gridsearch;k=11;t=0.000000;Micro-averaged F-Measure=0.626
method=ml-knn;mode=gridsearch;k=11;t=0.000000;Example-Based Accuracy=0.536
method=ml-knn;mode=gridsearch;k=11;t=0.000000;Example-Based F Measure=0.570
method=ml-knn;mode=gridsearch;k=1;t=0.000000;Hamming Loss=0.024

		 */
		List<Pair<MultiLabelLearner, String>> learners = new ArrayList<>();
		//always add...
		OneThreshold learner = new OneThreshold(new BinaryRelevance(new SMO()), new MicroFMeasure(dataset.getNumLabels()));
		learner.setDebug(true);
		learners.add(new Pair<>(learner, "br_smo"));
		
		List<String> lines = IOUtils.readFile(outputBestGS);
		for(String line: lines){
			//TODO: re-used threshold or not? makes difference?
			if(line.contains("iblr-knn")){
				String[] tokens = line.split(";");
				int k = Integer.valueOf(tokens[2].split("=")[1]);
				//double threshold = Double.valueOf(tokens[3].split("=")[1]);
				String metric = tokens[tokens.length-1].split("=")[0];
				learner = new OneThreshold(new IBLR_ML(k), new MicroFMeasure(dataset.getNumLabels()));
				learner.setDebug(true);
				learners.add(new Pair<>(learner, "iblr-knn " + metric + " k=" + k));
			}
			if(line.contains("ml-knn")){
				String[] tokens = line.split(";");
				int k = Integer.valueOf(tokens[2].split("=")[1]);
				//double threshold = Double.valueOf(tokens[3].split("=")[1]);
				String metric = tokens[tokens.length-1].split("=")[0];
				learner = new OneThreshold(new IBLR_ML(k), new MicroFMeasure(dataset.getNumLabels()));
				learner.setDebug(true);
				learners.add(new Pair<>(learner, "ml-knn " + metric + " k=" + k));
			}
		}
		return learners;
	}
	
	private String bestResult(List<String> keys, MultiKeyMap2<String, String, String> gridsearchResuls, String metric){
		Double max = -1.0;
		String maxKey = null;
		for(String key: keys){
			Double value = Double.valueOf(gridsearchResuls.get(key, metric));
			if(value > max){
				max = value;
				maxKey = key;
			}
		}
		return maxKey;
	}
	
	public void runOuter(File train, File test, File xml, List<Pair<MultiLabelLearner, String>> learners) throws Exception{
		runOuterLearners(train, test, xml, learners);
	}
	
	public List<Pair<MultiLabelLearner,String>>  getLearners(MultiLabelInstances dataset, int kRowRange[]) throws Exception {
		//int folds = 1; //inner folds for optimizing threshold... always 1/ CORRECT?
		int numLabels= dataset.getNumLabels();
		//ml-knn GS
		List<Pair<MultiLabelLearner,String>> learners = new ArrayList<>();
		for(int k: kRowRange){
			MultiLabelLearner learner = new OneThreshold(new MLkNN(k,1.0), new MicroFMeasure(numLabels)); //Ok OneThreshold? No option for label cardinality?
			learners.add(new Pair<>(learner, "ml-knn k=" + k));
		}
		//IBLR_ML
		for(int k: kRowRange){
			MultiLabelLearner learner = new OneThreshold(new IBLR_ML(k), new MicroFMeasure(numLabels));
			learners.add(new Pair<>(learner, "iblr_ml k=" + k));
		}
		//BR + log reg
		//MultiLabelLearner learner = new OneThreshold(new BinaryRelevance(new SMO()), new MicroFMeasure(numLabels));
		//learners.add(new Pair<>(learner, "br_smo"));
		return learners;
	}
	
	public File takeSample(File train, int size) throws IOException{
		File output = IOUtils.getFileWithDifferentSuffix(train, "-sample", "arff");
		IOUtils.stream2(train, output, new Streamer2() {

			private boolean data_section = false;
			private int row=0;

			@Override
			public String doLine(String line) {
				if(line.startsWith("@data")){
					data_section = true;
					return line;
				}
				if(data_section){
					row++;
					if(row < size)
						return line;
					return null;
				}
				return line;
			}
		}, false);
		return output;
	}
	
	private MultiKeyMap2<String, String, String> runInnerLearners(List<Pair<MultiLabelLearner,String>> learners,  MultiLabelInstances dataset, int numFolds){
		Evaluator evaluator = new Evaluator();
		//same set as for us
		List<Measure> measures = Arrays.asList(
				new MacroFMeasure(dataset.getNumLabels()),
				new MicroFMeasure(dataset.getNumLabels()), 
				new ExampleBasedFMeasure(),
				new ExampleBasedAccuracy(),
				new HammingLoss());
		Set<String> measureNames = measures.stream().map((m) -> m.getName()).collect(Collectors.toSet());
		MultiKeyMap2<String, String, String> all_results = new MultiKeyMap2<>();
		for(Pair<MultiLabelLearner,String> learnerLabelPair: learners){
			String learnerLabel = learnerLabelPair.getSecond();
			MultiLabelLearner learner = learnerLabelPair.getFirst();
			Timer start = new Timer("start " + learnerLabel + " #" + dataset.getNumInstances());
			MultipleEvaluation results = evaluator.crossValidate(learner, dataset, measures, numFolds);
			long milisElapsed = start.end();
			//System.out.println(results);
			String threshold = String.format("%.6f", ((OneThreshold)learner).getThreshold());
			for (Measure m : results.getEvaluations().get(0).getMeasures()) {
				String measureName = m.getName();
				if(!measureNames.contains(measureName))
					continue;
				String value = String.format("%.3f",results.getMean(measureName)); //, results.getStd(measureName));
				all_results.put(learnerLabel, measureName, value);
			}
			all_results.put(learnerLabel, "threshold", threshold);
			all_results.put(learnerLabel, "time milis", "" + milisElapsed);
			System.out.println(all_results.get(learnerLabel));
		} 
		System.out.println(all_results);
		return all_results;
	}
	
	private void runOuterLearners(File train, File test, File xml, List<Pair<MultiLabelLearner,String>> learners) throws Exception{
		MultiKeyMap2<String, String, String> all_results = new MultiKeyMap2<>();
		MultiLabelInstances trainInstances = new MultiLabelInstances(train.getAbsolutePath(),xml.getAbsolutePath());
		MultiLabelInstances testInstances = new MultiLabelInstances(test.getAbsolutePath(),xml.getAbsolutePath());
		//same set as for us
		List<Measure> measures = Arrays.asList(
				new MacroFMeasure(trainInstances.getNumLabels()),
				new MicroFMeasure(trainInstances.getNumLabels()), 
				new ExampleBasedFMeasure(),
				new ExampleBasedAccuracy(),
				new HammingLoss());
		Set<String> measureNames = measures.stream().map((m) -> m.getName()).collect(Collectors.toSet());
		for(Pair<MultiLabelLearner,String> pair: learners){
			String label = pair.getSecond();
			MultiLabelLearner learner = pair.getFirst();
			Timer start = new Timer("start " + label + "#train=" + trainInstances.getNumInstances() + " #test=" + testInstances.getNumInstances());
			learner.build(trainInstances);
			//Evaluation results  = MulanSparseEvaluator.evaluate(learner, testInstances, measures, -1, null);
			Evaluation results = new Evaluator().evaluate(learner, testInstances, measures); //measures?
			//System.out.println(results);
			long milisElapsed = start.end();
			for (Measure m : results.getMeasures()) {
				String measureName = m.getName();
				if(!measureNames.contains(measureName))
					continue;
				String value = String.format("%.4f", m.getValue());
				all_results.put(label, measureName, value);
			}
			all_results.put(label, "time milis", "" + milisElapsed);
			System.out.println(all_results.get(label));
		} 
		System.out.println(all_results);
	}
	

}