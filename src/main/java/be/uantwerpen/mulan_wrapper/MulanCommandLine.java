package be.uantwerpen.mulan_wrapper;

import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import mulan.classifier.MultiLabelLearner;
import mulan.classifier.lazy.IBLR_ML;
import mulan.classifier.lazy.MLkNN;
import mulan.classifier.meta.thresholding.OneThreshold;
import mulan.classifier.transformation.BinaryRelevance;
import mulan.data.MultiLabelInstances;
import mulan.evaluation.Evaluation;
import mulan.evaluation.Evaluator;
import mulan.evaluation.measure.ExampleBasedAccuracy;
import mulan.evaluation.measure.ExampleBasedFMeasure;
import mulan.evaluation.measure.HammingLoss;
import mulan.evaluation.measure.MacroFMeasure;
import mulan.evaluation.measure.Measure;
import mulan.evaluation.measure.MicroFMeasure;
import weka.classifiers.functions.SMO;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.util.CollectionUtils;

//motivation: since mulan algorithms seem to go out of memory...
//do this!
public class MulanCommandLine {

	//e.g. MulanCommandLine medical-train.arff  medical-test.arff medical.xml ml-knn 10
	//e.g. MulanCommandLine medical-train.arff  medical-test.arff medical.xml iblr 10
	//e.g. MulanCommandLine medical-train.arff  medical-test.arff medical.xml br-smo
	//saves one line to all_results_mulan.csv
	public static void main(String[] args) throws Exception{
		File datadir = new File("src/main/resources/multi-label-datasets/");
		String train = args[0];
		String test = args[1];
		String labels = args[2];
		String method = args[3];
		Integer k = (method.equals("ml-knn") || method.equals("ml-knn-pure") || method.equals("iblr"))? Integer.valueOf(args[4]):null;
		
		String learnerLabel = method + "-" + train + "-"  + (k!=null?k:"");

		File trainArff = new File(datadir, train);
		File testArff = new File(datadir, test);
		File labelsXML = new File(datadir, labels);

		//load data
		MultiLabelInstances datasetTrain =  new MultiLabelInstances(trainArff.getAbsolutePath(), labelsXML.getAbsolutePath());
		MultiLabelInstances datasetTest =  new MultiLabelInstances(testArff.getAbsolutePath(), labelsXML.getAbsolutePath());

		//make Learner:
		MultiLabelLearner learner = null;
		if(method.equals("ml-knn")){
			MultiLabelLearner base1 = new MLkNN(k,1.0); base1.setDebug(true);
			learner = new OneThreshold(base1, new MicroFMeasure(datasetTrain.getNumLabels())); learner.setDebug(true); 
		}
		else if(method.equals("ml-knn-pure")){
			learner = new MLkNN(k,1.0); learner.setDebug(true);
		}
		else if(method.equals("iblr")){
			MultiLabelLearner base2 = new IBLR_ML(k); base2.setDebug(true);
			learner = new OneThreshold(base2, new MicroFMeasure(datasetTrain.getNumLabels())); learner.setDebug(true); 
		}
		else if(method.equals("br-smo")){
			MultiLabelLearner base3 = new BinaryRelevance(new SMO()); base3.setDebug(true);
			learner = new OneThreshold(base3, new MicroFMeasure(datasetTrain.getNumLabels())); learner.setDebug(true); 
		}
		MultiLabelLearner learnerFinal = learner;

		//maintain: error + results
		boolean[] error = new boolean[]{false};
		String[] errorMsg = new String[]{""};
		Map<String, String> metricValues = new HashMap<String,String>();
		double[] time = new double[]{-1};
		//learn
		Runnable runnable = new Runnable(){

			@Override
			public void run() {
				try {
					//same set as for us
					List<Measure> measures = Arrays.asList(
							new MacroFMeasure(datasetTrain.getNumLabels()),
							new MicroFMeasure(datasetTrain.getNumLabels()), 
							new ExampleBasedFMeasure(),
							new ExampleBasedAccuracy(),
							new HammingLoss());
					Set<String> measureNames = measures.stream().map((m) -> m.getName()).collect(Collectors.toSet());
					Timer start = new Timer("start " + train + "#train=" + datasetTrain.getNumInstances() + " #test=" + datasetTest.getNumInstances());
					learnerFinal.build(datasetTrain);
					//Evaluation results  = MulanSparseEvaluator.evaluate(learner, testInstances, measures, -1, null);
					Evaluation results = new Evaluator().evaluate(learnerFinal, datasetTest, measures); //measures?
					//System.out.println(results);
					long milisElapsed = start.end();
					for (Measure m : results.getMeasures()) {
						String measureName = m.getName();
						if(!measureNames.contains(measureName))
							continue;
						String value = String.format("%.3f", m.getValue());
						metricValues.put(measureName, value);
					}
					time[0] = milisElapsed/(double)(60000);
					System.out.println("======== MULAN RESULT " + train + "======== ");
					System.out.println(metricValues);
				} catch (Exception e) {
					e.printStackTrace();
					error[0]=true;
					errorMsg[0] = e.getMessage();
				}
			}
		};
		
		//Start thread with timeout
		Thread t = new Thread(runnable);
		t.start();
		//long timeout = 1000 * 60 * 60 * 2; //max 2 hours
		//t.join(timeout);
		t.join(); //NO TIMEOUT
		/*
		boolean killed = false;
		if(t.isAlive()){
			System.err.println("KILLING THREAD " + learnerLabel + " AFTER 2 hours");
			killed = true;
			t.stop();
		}
		*/
		boolean killed = false;
		//save to 1 file
		FileWriter writer = new FileWriter("./temp/all_results_mulan.csv",true);
		String metricsStr = CollectionUtils.join(metricValues, "=",";");
		writer.write(String.format("dataset=%s;method=%s;k=%s;minutes=%.3f;killed=%s;error=%s;error_msg=%s;metrics=%s\n",
				train, method, k, time[0],killed,error[0],errorMsg[0],metricsStr));
		writer.close();
	}
}
