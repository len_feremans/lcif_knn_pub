package be.uantwerpen.lcif_knn;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import be.uantwerpen.util.Timer;
import be.uantwerpen.util.model.MyMinHeap;
import be.uantwerpen.util.model.Pair;

public class SecondRowKNNPredictions {

	public  LCIFPredictions makePredictionAllTestRows(LCIFData data, RowKNNCache cache, SecondRowKNNCache rowRowCache, int k1, int k2)
	{
		try{
			File file = new File("./temp/" + data.getDatasetName() + "-predictions-secondrow-knn-k=" + k1 + "_k2=" + k2 + ".txt");
			makePredictionAllTestRows(data,cache,rowRowCache,k1,k2,file);
			LCIFPredictions result = new LCIFPredictions();
			result.load(file);
			return result;
		}catch(IOException e){
			throw new RuntimeException(e);
		}
	}
	
	public  void makePredictionAllTestRows(LCIFData data, RowKNNCache cache, SecondRowKNNCache rowRowCache, int k1, int k2, File outputPredictionsFile) throws IOException {
		Timer timer = new Timer("makePredictionsSecondRow");
		List<Prediction> preferences = new ArrayList<>();
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputPredictionsFile));
		//predictions user-based
		for(int test_row=0; test_row<data.numberOfRowsInTest(); test_row++){
			if(test_row % 2000 == 0){
				timer.progress(test_row, data.numberOfRowsInTest());
			}
			preferences = makePredictionRow(data, cache, rowRowCache, test_row, k1, k2);
			RowKNNPredictions.savePrediction(writer, test_row, preferences);
		}
		writer.close();
		timer.end();
	}
	
	public  List<Prediction> makePredictionRow(LCIFData data, RowKNNCache cache, SecondRowKNNCache rowRowCache, int test_id, int k1, int k2) 
	{
		//compute 'weighted' second order neighbours
		MyMinHeap<Double, Integer> secondOrderNeighbours = new MyMinHeap<Double, Integer>(k2);
		List<Neighbor> neighborsL1 = cache.getSimmilarRowsTopK(data, test_id, k1);
		for(Neighbor neighborL1: neighborsL1){
			List<Neighbor> neighborsL2 = rowRowCache.getSimmilarRowsTopK(data, neighborL1.getTrainingRowId(), k2);
			for(Neighbor neighborL2: neighborsL2){
				secondOrderNeighbours.add(neighborL1.getSimilarity() * neighborL2.getSimilarity(), neighborL2.getTrainingRowId()); //simil (test-user, training_user_l2) = simil-features(test-user,training-user-l1) * simil-features-and-labels(trainig-user-l1,training-user-l2);
			}
		}
		List<Pair<Double,Integer>> secondOrderNeighboursTopK  = secondOrderNeighbours.values();
		
		//compute preference score for each label
		Map<Integer,Double> sums = new HashMap<Integer,Double>();
		for(Pair<Double,Integer> neigbour: secondOrderNeighboursTopK){
			int[] labels =  data.getTrainingRowLabels(neigbour.getSecond());
			for(Integer label: labels){
				Double sum = sums.get(label);
				sum = (sum == null)? 0.0: sum;
				sums.put(label, sum + neigbour.getFirst());
			}
		}
		List<Prediction> preferences = new ArrayList<>();
		double denominator = 0.0;
		for(Pair<Double,Integer> neigbour: secondOrderNeighboursTopK){
			denominator += neigbour.getFirst();
		}
		for(Entry<Integer,Double> labelSum: sums.entrySet()){
			preferences.add(new Prediction(labelSum.getKey(), labelSum.getValue()/denominator));
		}
		//sort
		Collections.sort(preferences, RowKNNPredictions.highToLow);
		return preferences;
	}

}
