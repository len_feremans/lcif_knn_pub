package be.uantwerpen.lcif_knn;

import java.util.Collections;
import java.util.List;

import be.uantwerpen.util.model.ListMap;

public class FeatureKNNCache {

	//Could use other datastructures here, e.g. Trove?
	private ListMap<Integer, FeatureNeighbor> cache;
	
	public void setCache(ListMap<Integer, FeatureNeighbor> cache){
		this.cache = cache;
	}
	
	public List<FeatureNeighbor> getSimilarLabelsTopK(LCIFData data, int feature_id, int k){
		List<FeatureNeighbor> neighbours = cache.get(feature_id);
		if(neighbours == null)
			return Collections.emptyList();
		return neighbours.subList(0, Math.min(neighbours.size(), k));
	}
}
