package be.uantwerpen.lcif_knn;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import mulan.evaluation.measure.InformationRetrievalMeasures;
import be.uantwerpen.util.model.CountMap;
import be.uantwerpen.util.util.MathUtils;

public class LCIFEvaluation {
	
	public static class EvaluationMetricValue{
		public static String MACRO_F1 = "Label macro F1";
		public static String MICRO_F1 = "Label micro F1";
		public static String F1 = "Example F1";
		public static String ACCURACY = "Example Accuracy";
		public static String HAMMING_LOSS = "Example Hamming Loss";
		public String name;
		public double value;
		
		public EvaluationMetricValue(String name, double value) {
			super();
			this.name = name;
			this.value = value;
		}
		
		public String toString(){
			return String.format("%s=%.3f", name, value);
		}
	}
	
	private boolean useIfNoLabelPredictHighestHeuristic = true;
	
	public void setUseNoLabelPredictHighesHeuristic(){
		this.useIfNoLabelPredictHighestHeuristic = false;
	}
	
	public List<EvaluationMetricValue> computeMetricValues(LCIFData data, LCIFPredictions predictions){
		return Arrays.asList(new EvaluationMetricValue[]{
				new EvaluationMetricValue(EvaluationMetricValue.MACRO_F1, computeMacroF1(data, predictions)),
				new EvaluationMetricValue(EvaluationMetricValue.MICRO_F1, computeMicroF1(data, predictions)),
				new EvaluationMetricValue(EvaluationMetricValue.F1, computeExampleBasedF1(data, predictions)),
				new EvaluationMetricValue(EvaluationMetricValue.ACCURACY, computeExampleBasedAccuracy(data, predictions)),
				new EvaluationMetricValue(EvaluationMetricValue.HAMMING_LOSS, computeExampleBasedHammingLos(data, predictions))
		});
	}
	
	public void printPredictionsWithNoLabels(LCIFData data, LCIFPredictions predictions){
		int numberOf0labelsInTest = 0;
		int numberOf0labelsInPredictions = 0;
		for(int i=0; i< data.numberOfRowsInTest(); i++){
			int[] labels = data.getTestRowLabels(i);
			if(labels.length == 0){
				numberOf0labelsInTest++;
			}
			long noPredictions = predictions.numberOfRelevantLabels(i);
			if(noPredictions == 0){
				numberOf0labelsInPredictions++;
			}
		}
		System.out.format("#test instances: %d; #test instance with no label: %d; #predictions with no labels: %d; \n",  
				data.numberOfRowsInTest(), numberOf0labelsInTest, numberOf0labelsInPredictions);
	}
	
	public double computeOptimalSingleThresholdLabelCardinality(LCIFData data, LCIFPredictions predictions){
		//One Threhold: minimize label cardinality 
		double labelCardinality = data.getLabelCardinalityInTraining(); // WAS test, as defined by Jessy Read, but seems like sheating.. todo: evaluate
		double bestThreshold = -1;
		double bestThresholdCardinality = 1000000;
		//first sweep 
		for(double threshold: MathUtils.makeRangeIncl(0, 1.0, 0.1)){
			LCIFPredictions preds = predictions.doOneThreshold(threshold);
			double d = getLabelCardinality(preds);
			if(Math.abs(d - labelCardinality) < Math.abs(bestThresholdCardinality - labelCardinality)){
				bestThreshold = threshold;
				bestThresholdCardinality = d;
			}
		}
		//second sweep
		for(double threshold: MathUtils.makeRangeIncl(bestThreshold-0.1, bestThreshold+0.1, 0.01)){
			LCIFPredictions preds = predictions.doOneThreshold(threshold);
			double d = getLabelCardinality(preds);
			if(Math.abs(d - labelCardinality) < Math.abs(bestThresholdCardinality - labelCardinality)){
				bestThreshold = threshold;
				bestThresholdCardinality = d;
			}
		}
		return bestThreshold;
	}
	
	public double getLabelCardinality(LCIFPredictions predictions){
		int noLabels = 0;
		for(int i=0; i< predictions.getNumberOfPredictions(); i++){
			noLabels += predictions.numberOfRelevantLabels(i);
		}
		return noLabels / (double)predictions.getNumberOfPredictions();
	}
	
	public double computeExampleBasedHammingLos(LCIFData data, LCIFPredictions predictions){
		double hammingLoss = 0.0;
		int noLabels = data.numberOfLabels(); //ok? or (actual) numberOfLabelsTest 
		for(int i=0; i< data.numberOfRowsInTest(); i++){
			int[] labels = data.getTestRowLabels(i);
			//intersection size:
			Set<Integer> set = new HashSet<Integer>();
			for(int label: labels)
				set.add(label);
			int intersectionSize = predictions.intersectionPreferencesAndLabels(set, i);
			double symmDiff = labels.length + predictions.numberOfRelevantLabels(i) - intersectionSize - intersectionSize;
			//special case for top label if no predictions after thresholding
			if(useIfNoLabelPredictHighestHeuristic && predictions.numberOfRelevantLabels(i) == 0){
				if(predictions.getPredictionsRaw(i).size() > 0){
					int bestLabel = predictions.getPredictionsRaw(i).get(0).getLabelId();
					intersectionSize = set.contains(bestLabel)?1:0;
					symmDiff =labels.length + 1 - intersectionSize*2;
				}
			}
			hammingLoss += symmDiff / noLabels;
		}
		hammingLoss = hammingLoss/(double) data.numberOfRowsInTest();
		//timer.end();
		return hammingLoss;
	}
	
	public double computeExampleBasedAccuracy(LCIFData data, LCIFPredictions predictions){
		double accuracy = 0.0;
		for(int i=0; i< data.numberOfRowsInTest(); i++){
			int[] labels = data.getTestRowLabels(i);
			//intersection size:
			Set<Integer> set = new HashSet<Integer>();
			for(int label: labels)
				set.add(label);
			int intersectionSize = predictions.intersectionPreferencesAndLabels(set, i);
			int unionSize = labels.length + predictions.numberOfRelevantLabels(i) - intersectionSize;
			double acc_instance = 0.0;
			if(unionSize != 0)
				acc_instance= intersectionSize/(double)unionSize;
			//special case for top label if no predictions after thresholding
			if(useIfNoLabelPredictHighestHeuristic && predictions.numberOfRelevantLabels(i) == 0){
				if(predictions.getPredictionsRaw(i).size() > 0){
					int bestLabel = predictions.getPredictionsRaw(i).get(0).getLabelId();
					intersectionSize = set.contains(bestLabel)?1:0;
					unionSize = labels.length + 1 - intersectionSize;
					if(unionSize != 0)
						acc_instance= intersectionSize/(double)unionSize;
				}
			}
			//see also: http://stats.stackexchange.com/questions/12702/what-are-the-measure-for-accuracy-of-multilabel-data
			accuracy += acc_instance;
		}
		accuracy = accuracy/(double) data.numberOfRowsInTest();
		//timer.end();
		return accuracy;
	}
	
	public double computeExampleBasedF1(LCIFData data, LCIFPredictions predictions){
		double f1 = 0.0;
		for(int i=0; i< data.numberOfRowsInTest(); i++){
			int[] labels = data.getTestRowLabels(i);
			//intersection size:
			Set<Integer> set = new HashSet<Integer>();
			for(int label: labels)
				set.add(label);
			int intersectionSize = predictions.intersectionPreferencesAndLabels(set, i);
			int denominator = labels.length + predictions.numberOfRelevantLabels(i);
			double f1_instance = 0.0;
			if(denominator != 0)
				f1_instance= (2 * intersectionSize)/(double)denominator;
			//special case for top label if no predictions after thresholding
			if(useIfNoLabelPredictHighestHeuristic && predictions.numberOfRelevantLabels(i) == 0){
				if(predictions.getPredictionsRaw(i).size() > 0){
					int bestLabel = predictions.getPredictionsRaw(i).get(0).getLabelId();
					intersectionSize = set.contains(bestLabel)?1:0;
					denominator = labels.length + 1;
					if(denominator != 0)
						f1_instance= (2 * intersectionSize)/(double)denominator;
				}
			}
			f1 += f1_instance;
		}
		f1 = f1/(double) data.numberOfRowsInTest();
		//timer.end();
		return f1;
	}
	
	public class ConfusionMatrixMultiLabel{
//		CountMap<Integer> any = new CountMap<>();
		CountMap<Integer> labelTP = new CountMap<>();
		CountMap<Integer> labelFP = new CountMap<>();
		CountMap<Integer> labelFN = new CountMap<>();
	}

	public  double computeMacroF1(LCIFData data,LCIFPredictions predictions){
		ConfusionMatrixMultiLabel matrix = buildConfusionMatrix(data, predictions);;
		double macroF1 = 0;
		double labelSize = data.numberOfLabels(); //data.numberOfLabelsInTest();
		//bug: loop over ALL labels... if label does not occur in any test instance or predictions... precision/recall will be 1.0
		Set<Integer> allLabels = data.getAllLabels();
		for(int label: allLabels){
			double f1 = 0.0;
			double tp = matrix.labelTP.get(label);
			double fn = matrix.labelFN.get(label);
			double fp = matrix.labelFP.get(label);
			f1 = InformationRetrievalMeasures.fMeasure(tp, fp, fn, 1.0);//beetje weird... als alle drie 0 -> precision is 1.0
			//BUG: labelSize +=1;
			macroF1 += f1;
		}
		macroF1 = macroF1 / (double)labelSize;
		//BUG: DIFFERENT FROM MULAN
		return macroF1;
	}
	

	public  double computeMicroF1(LCIFData data, LCIFPredictions predictions){
		ConfusionMatrixMultiLabel matrix = buildConfusionMatrix(data, predictions);
		double tp_total = 0;
		double fp_total = 0;
		double fn_total = 0;
		for(int label: data.getAllLabels()){
			double tp = matrix.labelTP.get(label);
			double fn = matrix.labelFN.get(label);
			double fp = matrix.labelFP.get(label);
			tp_total += tp;
			fp_total += fp;
			fn_total += fn;
		}
		double microPrecision = InformationRetrievalMeasures.precision(tp_total, fp_total, fn_total);
		double microRecall = InformationRetrievalMeasures.recall(tp_total, fp_total, fn_total);
		double macroF1= 2 * microPrecision * microRecall / (microPrecision + microRecall);
		if(microPrecision + microRecall== 0.0){
			macroF1 = 0.0;
		}
		return macroF1;
	}

	private ConfusionMatrixMultiLabel buildConfusionMatrix(LCIFData data, LCIFPredictions predictions) {
		ConfusionMatrixMultiLabel matrix = new ConfusionMatrixMultiLabel();
		for(int i=0; i< data.numberOfRowsInTest(); i++){
			Set<Integer> set = new HashSet<Integer>();
			Set<Integer> prefsSet = new HashSet<Integer>();
			int[] labels = data.getTestRowLabels(i);
			for(int label: labels){
				set.add(label);
			}
			List<Prediction> predictionsNoThreshold = predictions.getPredictionsRaw(i);
			for(Prediction pref: predictionsNoThreshold){
				if(!Double.isNaN(predictions.getThreshold()) && pref.getScore() > predictions.getThreshold())
					prefsSet.add(pref.getLabelId());
				else if(Double.isNaN(predictions.getThreshold())){
					prefsSet.add(pref.getLabelId());
				}
			}
			//special case for top label if no predictions after thresholding
			if(useIfNoLabelPredictHighestHeuristic && predictions.numberOfRelevantLabels(i) == 0){
				if(predictions.getPredictionsRaw(i).size() > 0){
					int bestLabel = predictions.getPredictionsRaw(i).get(0).getLabelId();
					prefsSet.add(bestLabel);
				}
			}
			for(Integer labelPredicted: prefsSet){
				if(set.contains(labelPredicted)){
					matrix.labelTP.add(labelPredicted);
				}
				else{
					matrix.labelFP.add(labelPredicted);
				}
			}
			for(int label: labels){
				if(!prefsSet.contains(label)){
					matrix.labelFN.add(label);
				}
			}
		}
		return matrix;
	}

	public void saveFileWithResults(LCIFData data, LCIFPredictions predictions, File outputEvaluationCSV) throws IOException{
		ConfusionMatrixMultiLabel matrix = buildConfusionMatrix(data, predictions);
		FileWriter writer = new FileWriter(outputEvaluationCSV);
		List<Integer> allLabels = new ArrayList<Integer>(data.getAllLabels());
		List<Integer> labelsSortedOnSupport = allLabels.stream().
				sorted((l1, l2) -> 
				{
					int countL1 = data.getFrequencyLabelInTraining(l1);
					int countL2 = data.getFrequencyLabelInTraining(l2);
					return countL2-countL1;
				}).
				collect(Collectors.toList());
		writer.write(String.format("label,support (training), occurences(test), TP,FP,FN,precision,recall,f1\n"));
		for(Integer label: labelsSortedOnSupport){
			double precision =  0.0;
			double recall = 0.0;
			double f1= 0.0;
			if(matrix.labelTP.get(label) > 0){
				precision = matrix.labelTP.get(label) / (double)(matrix.labelTP.get(label) + matrix.labelFP.get(label)); //tp / tp + fp
				recall =  matrix.labelTP.get(label) / (double)(matrix.labelTP.get(label) + matrix.labelFN.get(label)); //tp / tp + fn
				f1= 2 * precision * recall / (precision + recall);
			}
			int countTraining =  data.getFrequencyLabelInTraining(label);
			writer.write(String.format("%-5d,%-5d,%-5d,%-5d,%-5d,%-5d,%-5.3f,%-5.3f,%-5.3f\n", 
					//todo: fix for countTest each label
					label, countTraining, -1, matrix.labelTP.get(label), matrix.labelFP.get(label), matrix.labelFN.get(label),
					precision, recall, f1));
		}
		for(int[] range: new int[][]{{0,0},{1,1},{2,2},{3,3},{4,4},{5,5},{6,10},{11,20},{21,30},{31,100},{100,500},{500,1000}}){
			double macroPrecision = 0;
			double macroRecall = 0;
			double labelSize = 0;
			double macroF1 = 0;
			int countTrainingTotal = 0;
			for(Integer label: labelsSortedOnSupport){
				int countTraining = data.getFrequencyLabelInTraining(label); 
				if(countTraining >= range[0] && countTraining <= range[1]){
					labelSize +=1;
					double precision =  0.0;
					double recall = 0.0;
					if(matrix.labelTP.get(label) > 0){
						precision = matrix.labelTP.get(label) / (double)(matrix.labelTP.get(label) + matrix.labelFP.get(label)); //tp / tp + fp
						recall =  matrix.labelTP.get(label) / (double)(matrix.labelTP.get(label) + matrix.labelFN.get(label)); //tp / tp + fn
					}
					macroPrecision += precision;
					macroRecall += recall;	
					countTrainingTotal += countTraining;
				}
			}
			if(labelSize > 0){
				macroPrecision = macroPrecision / (double)labelSize;
				macroRecall = macroRecall / (double)labelSize;
				macroF1= 2 * macroPrecision * macroRecall / (macroPrecision + macroRecall);
			}
			System.out.format("labelgroup with support between %-5d-%-5d: no labels:%d, pre: %-5.3f, recall: %-5.3f, f1:%-5.3f\n", 
					range[0], range[1], labelSize,
					macroPrecision, macroRecall, macroF1);
		}
		writer.close();
	}
	

	//for testing
	public void printBestPredictionAssumingOracle(LCIFData data, LCIFPredictions predictions) throws IOException {	
		//output:
		//global accuracy
		double maxAcc = -1;
		double maxT = -1;
		for(double t: MathUtils.makeRange(0.0, 1.0, 0.1)){
			double accuracy = computeExampleBasedAccuracy(data, predictions.doOneThreshold(t));
			if(accuracy > maxAcc){
				System.out.format("    DEBUG: ... accuracy is %.3f (threshold: %.3f)\n", accuracy, t);
				maxAcc = accuracy;
				maxT = t;
				
			}
		}
		System.out.format("oracle accuracy is %.3f (threshold: %.3f)\n", maxAcc, maxT);
		
		//per class: precision/recall + noClasses
		//macro accuracy
		double maxF1 = -1;
		for(double t: MathUtils.makeRange(0.0, 1.0, 0.1)){
			double macroF1 = computeMacroF1(data , predictions.doOneThreshold(t));
			if(macroF1 > maxF1){
				System.out.format("    DEBUG... macro f1 is %.3f (threshold: %.3f)\n", macroF1, t);
				maxF1 = macroF1;
				maxT = t;
			}
		}
		System.out.format("oracle macro f1 is %.3f (threshold: %.3f)\n", maxF1, maxT);
	}


}
