package be.uantwerpen.lcif_knn;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import be.uantwerpen.util.Timer;
import be.uantwerpen.util.model.CountMap;
import be.uantwerpen.util.util.IOUtils;
import be.uantwerpen.util.util.IOUtils.Streamer;
import be.uantwerpen.util.util.MathUtils;

public class LCIFData {

	private String datasetName;
	
	public int[][] training_data_features;
	public int[][] training_data_labels;
	
	private int[][] test_data_features;
	private int[][] test_data_labels; 
	
	private int[][] index_features; //e.g. feature 89494 has [1,2,3,4,5] meany example 1-5 contain index
	private int[][] index_labels; //e.g. feature 89494 has [1,2,3,4,5] meany example 1-5 contain index
								  //note: direct access here, so most arrays or null
	
	public LCIFData(){}
	
	public LCIFData(File training, File test){
		try{
			this.datasetName = training.getName();
			loadData(training, true);
			loadData(test, false);
			makeIndex();
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}
	
	public String getDatasetName(){
		return this.datasetName;
	}
	
	public void loadData(File libsvmdata, boolean training) throws IOException{
		if(this.datasetName == null)
			this.datasetName = libsvmdata.getName();
		Timer timer = new Timer("loading " + libsvmdata.getName());
		//parse data
		//Id,Data
		//1,0 1637693:1 1988219:1 1428563:1 117096:1 836227:
		//TODO: Less efficient... first List<List<Integer>>, then int[][]...Use Trove?
		List<List<Integer>> data_features = new ArrayList<>();
		List<List<Integer>> data_labels = new ArrayList<>();
		Pattern p = Pattern.compile("(\\s)+|(\\s)*(\\,)(\\s)*");

		IOUtils.stream(libsvmdata, new Streamer() {

			int lineNo = 0;
			@Override
			public void doLine(String line) {
				lineNo++;
				if(lineNo % 100000 == 0){
					timer.progress(lineNo);
				}
				//System.out.format("%d: %s\n", lineNo, line);
				String[] tokens = p.split(line);
				//parse labels
				List<Integer> labels = new ArrayList<Integer>();
				for(String token: tokens){
					if(token.length() == 0)
						continue;
					if(token.contains(":")){
						break;
					}
					else{
						labels.add(Integer.valueOf(token));
					}
				}
				data_labels.add(labels);
				//parse features
				List<Integer> features = new ArrayList<Integer>();
				for(String token: tokens){
					if(!token.contains(":")){
						continue;
					}
					else{
						features.add(Integer.valueOf(token.split(":")[0]));
					}
				}
				data_features.add(features);
			}});
		if(training){
			this.training_data_features = list2arr(data_features);
			this.training_data_labels = list2arr(data_labels);
		}
		else{
			this.test_data_features = list2arr(data_features);
			this.test_data_labels = list2arr(data_labels);
		}
		timer.end();
	}
	
	private int[][] list2arr(List<List<Integer>> lst){
		int[][] arr = new int[lst.size()][];
		for(int i=0; i<lst.size(); i++){
			arr[i] = new int[lst.get(i).size()];
			for(int j=0; j < arr[i].length; j++){
				arr[i][j] = lst.get(i).get(j);
			}
		}
		return arr;
	}
	
	
	public void makeIndex(){
		this.index_features = makeIndex(this.training_data_features);
		this.index_labels = makeIndex(this.training_data_labels);
	}
	
	private  int[][] makeIndex(int[][] data){
		Timer timer = new Timer("makeIndex");
		CountMap<Integer> countMap = new CountMap<Integer>();
		for(int row=0; row<data.length; row++){
			if(row % 20000 == 0){
				timer.progress(row, data.length);
			}
			int[] features = data[row];
			for(int feature=0; feature<features.length; feature++){
				int featureValue = features[feature];
				countMap.add(featureValue);
			}
		}
		timer.progress("counts",1);
		Set<Integer> featureSet = countMap.keySet();
		int min = MathUtils.minInt(featureSet);
		int max = MathUtils.maxInt(featureSet);
		System.out.format("Creating index: %d size. min: %d, max: %d\n", featureSet.size(), min, max);
		//alloc
		int[][] index = new int[max+1][];
		for(Integer feature: featureSet){
			index[feature] = new int[countMap.get(feature)];
		}
		timer.progress("alloc",1);
		//fill
		for(int row=0; row<data.length; row++){
			for(int feature=0; feature<data[row].length; feature++){
				int featureVal = data[row][feature];
				int freepos = indexOf(0, index[featureVal]);
				index[featureVal][freepos] = row;
			}
		}
		timer.end();
		//System.out.println(countMap);
		//GC
		countMap = null; featureSet = null;
		System.gc();
		return index;
	}
	
	public double getLabelCardinalityInTest(){
		int noLabels = 0;
		for(int i=0; i< this.test_data_labels.length; i++){
			if(test_data_labels[i] != null){
				noLabels += test_data_labels[i].length;
			}
		}
		return noLabels / (double)test_data_labels.length;
	}
	
	
	public double getLabelCardinalityInTraining(){
		int noLabels = 0;
		for(int i=0; i< this.training_data_labels.length; i++){
			if(training_data_labels[i] != null){
				noLabels += training_data_labels[i].length;
			}
		}
		return noLabels / (double)training_data_labels.length;
	}
	
	public double getFeatureCardinality(){
		int noFeatures = 0;
		for(int i=0; i< this.training_data_labels.length; i++){
			if(training_data_features[i] != null){
				noFeatures += training_data_features[i].length;
			}
		}
		return noFeatures / (double)training_data_features.length;
	}
	
	public double getAverageNumberOfInstancesPerFeature(){
		double noFeatures = 0;
		double noRowsForEachFeature = 0;
		for(int i=0; i< this.index_features.length; i++){
			if(this.index_features[i] != null){
				noFeatures++;
				noRowsForEachFeature += this.index_features[i].length;
			}
		}
		return noRowsForEachFeature/noFeatures;
	}
	
	public void printDataStats(){
		//compute maxLabel
		int maxLabel = 0;
		int minLabel = Integer.MAX_VALUE;
		if(index_labels != null){
			for(int i=0; i< index_labels.length; i++){
				if(index_labels[i] != null){
					maxLabel = Math.max(maxLabel, index_labels[i].length);
					minLabel = Math.min(minLabel, index_labels[i].length);
				}
			}
		}
		System.out.println("N;N_train;N_test;F;L;LCard;FCard;Pmax;AverageNumberOfInstanceForFeature;");
		System.out.format("%d;%d;%d;%d;%d;%.3f;%.3f;%.3f;%.3f\n",
				numberOfRowsInTraining() + numberOfRowsInTest(),
				numberOfRowsInTraining(),
				numberOfRowsInTest(),
				numberOfFeatures(),
				numberOfLabels(),
				getLabelCardinalityInTraining(),
				getFeatureCardinality(),
				maxLabel / (double) numberOfRowsInTraining(),
				getAverageNumberOfInstancesPerFeature());
	}
	
	private final  int indexOf(int a, int[] arr){
		for(int i=0; i<arr.length; i++){
			if(arr[i] == a){
				return i;
			}
		}
		throw new RuntimeException("full arr");
	}
	
	
	//ACCES DATA
	public int numberOfRowsInTraining(){
		return this.training_data_features.length;
	}
	
	public int numberOfFeatures(){
		Set<Integer> hashset = new HashSet<>();
		for(int[] instance: this.training_data_features){
			for(int f: instance){
				hashset.add(f);
			}
		}
		for(int[] instance: this.test_data_features){
			for(int f: instance){
				hashset.add(f);
			}
		}
		return hashset.size();
	}

	public int numberOfFeaturesInTraining() {
		return this.index_features.length;
	}	
	
	public int numberOfLabelsInTraining() {
		return this.index_labels.length;
	}	
	
	public int numberOfLabelsInTest(){
		Set<Integer> hashset = new HashSet<>();
		for(int[] instance: this.test_data_labels){
			for(int f: instance){
				hashset.add(f);
			}
		}
		return hashset.size();
	}
	
	public int numberOfLabels(){
		return getAllLabels().size();
	}
	
	public Set<Integer> getAllLabels(){
		Set<Integer> hashset = new HashSet<>();
		for(int[] instance: this.training_data_labels){
			for(int f: instance){
				hashset.add(f);
			}
		}
		for(int[] instance: this.test_data_labels){
			for(int f: instance){
				hashset.add(f);
			}
		}
		return hashset;
	}
	
	public int numberOfRowsInTest(){
		return this.test_data_features.length;
	}
	
	private static int empty_array[] = new int[]{};
	public int[] getTrainingRowsHaving(int test_feature){
		if(test_feature >= this.index_features.length || this.index_features[test_feature] == null){ //e.g. feature only occuring in test
			return empty_array;
		}
		return this.index_features[test_feature];
	}
	
	public int getFrequencyLabelInTraining(int label_id){
		if(label_id >= this.index_labels.length || this.index_labels[label_id] == null){ //e.g. label only occuring in test
			return 0;
		}
		return this.index_labels[label_id].length;
	}
	
	public int[] getTrainingRowsHavingLabel(int label){
		return this.index_labels[label];
	}
	
	public int[] getTrainingRowFeatures(int row_id){
		return this.training_data_features[row_id];
	}
	
	public int[] getTrainingRowLabels(int row_id){
		return this.training_data_labels[row_id];
	}

	public int[] getTestRowFeatures(int row_id){
		return this.test_data_features[row_id];
	}
	
	public int[] getTestRowLabels(int row_id){
		return this.test_data_labels[row_id];
	}

}
