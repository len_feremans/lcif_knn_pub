package be.uantwerpen.lcif_knn;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import be.uantwerpen.util.Timer;
import be.uantwerpen.util.model.CountMap;

public class FeatureKNNPredictions {

	public LCIFPredictions makePredictionAllTestRows(LCIFData data, FeatureKNNCache cache, int k) {
		try{
			File file = new File("./temp/" + data.getDatasetName() + "-predictions-feature-knn-k=" + k + ".txt");
			makePredictionAllTestRows(data,cache,k,file);
			LCIFPredictions result = new LCIFPredictions();
			result.load(file);
			return result;
		}catch(IOException e){
			throw new RuntimeException(e);
		}
	}
	
	public void makePredictionAllTestRows(LCIFData data, FeatureKNNCache cache, int k, File outputPredictionsFile) throws IOException {
		Timer timer = new Timer("makePredictionsItem");
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputPredictionsFile));
		for(int test_row=0; test_row< data.numberOfRowsInTest(); test_row++){
			if(test_row % 2000 == 0){
				timer.progress(test_row, data.numberOfRowsInTest());
				Runtime rt = Runtime.getRuntime();
				System.out.print("Memory:" + (rt.totalMemory() - rt.freeMemory()) / (1024*1024) + " Mb ");
				System.out.format("current row %d/%d: #prediction size: %d, #candidate-labels: %d\n", test_row, data.numberOfRowsInTest(), 
						lastPredictionsSize, lastNeighborLabelsSize);
			}
			List<Prediction> preferences = makePredictionRow(data, cache, test_row, k);
			RowKNNPredictions.savePrediction(writer, test_row, preferences);
		}
		writer.close();
		System.out.println("Saved " + outputPredictionsFile.getAbsolutePath());
		timer.end();
	}

	private Map<Integer,Double> sums = new HashMap<Integer,Double>();
	private CountMap<Integer> total = new CountMap<>();
	private List<Prediction> preferences = new ArrayList<>();
	
	private int lastNeighborLabelsSize = 0; 
	private int lastPredictionsSize = 0;
	
	public  List<Prediction> makePredictionRow(LCIFData data, FeatureKNNCache cache, int test_id, int k){
		//compute
		sums.clear();
		total.clear();
		preferences.clear();
		int[] features = data.getTestRowFeatures(test_id);
		for(int feature: features){
			List<FeatureNeighbor> neighborLabels = cache.getSimilarLabelsTopK(data, feature, k); //can be size 0 , e.g. feature only in test
			lastNeighborLabelsSize = neighborLabels.size();
			//get F1 <> SIMIL(L1), SIMIL(L2)
			//get F2 <> SIMIL(L1), SIMIL(L3)...
			//score item-based for L1 = &(F1) * SIMIL(F1,L1) + &(F2) * SIMIL(F2,L2) + ... 
			for(FeatureNeighbor nb: neighborLabels){
				Double sum = sums.get(nb.getLabelId());
				if(sum == null){
					sum = 0.0;
				}
				sum += nb.getSimilarity();
				sums.put(nb.getLabelId(), sum);
				total.add(nb.getLabelId());
			}
		}
		//compute final preferences
		for(Entry<Integer,Double> entry: sums.entrySet()){
			preferences.add(new Prediction(entry.getKey(), entry.getValue()/(double)total.get(entry.getKey()))); //OK: normalisatie ok
		}
		lastPredictionsSize = preferences.size();
		//sort
		Collections.sort(preferences, RowKNNPredictions.highToLow);
		return preferences;
	}
}