package be.uantwerpen.lcif_knn;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import be.uantwerpen.util.Timer;
import be.uantwerpen.util.model.CountMap;
import be.uantwerpen.util.model.MyMinHeap;


public class SecondRowKNNSearch {

	public SecondRowKNNCache computeAllSimilarRows(LCIFData data, final int k){
		try {
			File file = File.createTempFile("second-similar-knn", "cache");
			computeAllSimilarRows(data,k,file);
			SecondRowKNNCache cache = new SecondRowKNNCache();
			cache.load(file);
			return cache;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}		
	
	//compute closest neighbor for all test instances (copy-pasted RowKNNSearch)
	public void computeAllSimilarRows(LCIFData data, final int k, final File neigbhorFile) throws IOException {
		Timer timer = new Timer("computeAllSimilarRows-second-knn");	
		long total = 0;
		BufferedWriter writer  = new BufferedWriter(new FileWriter(neigbhorFile));
		for(int training_row=0; training_row<data.numberOfRowsInTraining(); training_row+=1){ //NOT TEST ROWS HERE!
			if(training_row % 1000 == 0)
				timer.progress(training_row, data.numberOfRowsInTraining());
			MyMinHeap<Double, Integer> neighbours = computeSimmilarRowsTopK(data, training_row, k);
			total += neighbours.size();
			RowKNNSearch.saveNeighboursRow(writer, training_row, neighbours.values()); 
		}
		writer.close();
		timer.end();
		System.out.println("Saved #neighbours: " + total + ", for training instances: " + data.numberOfRowsInTraining() + " ratio: " + total / (double)data.numberOfRowsInTraining());
	}
	
	//Uses cosine similarity of binary vector 
	public  MyMinHeap<Double, Integer> computeSimmilarRowsTopK(LCIFData data, int training_id, int maxK){
		int[] training_features = data.getTrainingRowFeatures(training_id);
		int[] training_labels = data.getTrainingRowLabels(training_id);
		
		double normT1 = RowKNNSearch.norm(training_features.length + training_labels.length);
		//double normT1 = RowKNNSearch.norm(training_features.length + training_labels.length * alpha);
		CountMap<Integer> rowCountsFeatures = new CountMap<>();
		for(int training_feature: training_features){
			int[] rows = data.getTrainingRowsHaving(training_feature);
			for(int row: rows){
				rowCountsFeatures.add(row);
			}
		}
		CountMap<Integer> rowCountLabels = new CountMap<>();
		for(int training_label : training_labels){
			int[] rows = data.getTrainingRowsHavingLabel(training_label);
			for(int row: rows){
				rowCountLabels.add(row);
			}
		}
		//remove 'self': No change 6-06-2017
		//rowCountsFeatures.getMap().remove(training_id);
		//rowCountLabels.getMap().remove(training_id);
		//simil:
		MyMinHeap<Double, Integer> myMinHeap = new MyMinHeap<Double, Integer>(maxK);
		Set<Integer> allRows = new HashSet<Integer>();
		allRows.addAll(rowCountsFeatures.keySet());
		allRows.addAll(rowCountLabels.keySet());
		for(Integer rowId: allRows){
			//double normT2 = RowKNNSearch.norm(data.getTrainingRowFeatures(rowId).length + data.getTrainingRowLabels(rowId).length * alpha);
			double normT2 = RowKNNSearch.norm(data.getTrainingRowFeatures(rowId).length + data.getTrainingRowLabels(rowId).length);
			int featuresInCommon =  rowCountsFeatures.get(rowId);
			int labelsInCommon =  rowCountLabels.get(rowId);
			//double cosineSimillarity = (featuresInCommon + labelsInCommon * alpha) / (normT2  * normT1);
			double cosineSimillarity = (featuresInCommon + labelsInCommon) / (normT2  * normT1);
			myMinHeap.add(cosineSimillarity, rowId);
		}
		return myMinHeap;
	}	
	
}
