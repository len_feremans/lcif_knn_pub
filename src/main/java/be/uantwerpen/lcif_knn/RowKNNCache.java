package be.uantwerpen.lcif_knn;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import be.uantwerpen.util.model.ListMap;
import be.uantwerpen.util.util.IOUtils;
import be.uantwerpen.util.util.IOUtils.Streamer;

public class RowKNNCache {
	
	private ListMap<Integer,Neighbor> cache;
	 
	public void load(File maxKsimilaritiesTestTraining) throws IOException{
		cache = parseNeighboursFile(maxKsimilaritiesTestTraining);
	}
	
	//note: test_id for classical RowKNN, but changes to training_id in context of second-row-knn
	public List<Neighbor> getSimmilarRowsTopK(LCIFData data, int test_id, int k){
		List<Neighbor> neighbours = cache.get(test_id);
		if(neighbours == null)
			return Collections.EMPTY_LIST;
		return neighbours.subList(0, Math.min(neighbours.size(), k));
	}
	
	private ListMap<Integer,Neighbor> parseNeighboursFile(File predictions) throws IOException{
		//parse prediction file
		//1,397598:0.449147, 76180:0.423968, 230296:0.423968,77828:0.408675,411873:0.408675,75145:0.391545,177054:0.232033,309676:0.232033,195253:0.232033,75859:0.232033,		
		ListMap<Integer,Neighbor> preferences = new ListMap<>();
		IOUtils.stream(predictions, new Streamer(){
			@Override
			public void doLine(String line) {
				String[] tokens = line.split("\\,");
				Integer test_id = Integer.valueOf(tokens[0]);
				for(int i=1; i<tokens.length; i++){
					Integer training_id = Integer.valueOf(tokens[i].split(":")[0]);
					Double simil = Double.valueOf(tokens[i].split(":")[1]);
					preferences.put(test_id, new Neighbor(training_id, simil));
				}
			}
		});
		return preferences;
	}
}
