package be.uantwerpen.lcif_knn;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import be.uantwerpen.util.util.CollectionUtils;
import be.uantwerpen.util.util.IOUtils;
import be.uantwerpen.util.util.IOUtils.Streamer;

public class LCIFPredictions {
	
	private List<List<Prediction>> predictions = new ArrayList<>();
	private double threshold = Double.NaN;
	
	public void load(File predictions) throws IOException{
		//parse prediction file
		//1,397598:0.449147, 76180:0.423968, 230296:0.423968,77828:0.408675,411873:0.408675,75145:0.391545,177054:0.232033,309676:0.232033,195253:0.232033,75859:0.232033,		
		List<List<Prediction>> preferences = new ArrayList<>();
		IOUtils.stream(predictions, new Streamer(){
			@Override
			public void doLine(String line) {
				String[] tokens = line.split("\\,");
				int test_id = Integer.valueOf(tokens[0]);
				if(test_id != preferences.size())
					throw new RuntimeException("Should not get here");
				List<Prediction> prefs = new ArrayList<>();
				for(int i=1; i<tokens.length; i++){
					int label = Integer.valueOf(tokens[i].split(":")[0]);
					double score = Double.valueOf(tokens[i].split(":")[1]);
					prefs.add(new Prediction(label,score));
				}
				preferences.add(prefs);
			}
			
		});
		this.predictions = preferences;
	}
	
	public String toString(){
		return CollectionUtils.join(predictions, "\n");
	}
	
	public void add(List<Prediction> predictions){
		this.predictions.add(predictions);
	}
	
	public List<List<Prediction>> getPredictionsRaw(){
		return this.predictions;
	}
	
	public long getNumberOfPredictions(){
		return this.predictions.size();
	}
	
	public int intersectionPreferencesAndLabels(Set<Integer> actualLabels, int i){
		int intersection = 0;
		for(Prediction pred: this.predictions.get(i)){
			if(!Double.isNaN(this.threshold) && pred.getScore() > this.threshold){
				if(actualLabels.contains(pred.getLabelId())){
					intersection++;
				}
			}
		}
		return intersection;
	}
	
	public int numberOfRelevantLabels(int i){
		int len = 0;
		if(Double.isNaN(this.threshold)){
			return this.predictions.get(i).size();
		}
		for(Prediction pred: this.predictions.get(i)){
			if(pred.getScore() > this.threshold){
				len++;
			}
		}
		return len;
	}
	
	public List<Prediction> getPredictionsRaw(int i){
		return this.predictions.get(i);
	}
	
	public double getThreshold(){
		return this.threshold;
	}


	public LCIFPredictions doOneThreshold(double t){
		LCIFPredictions predictions = new LCIFPredictions();
		predictions.predictions = this.predictions;
		predictions.threshold = t;
		return predictions;
	}
	
}
