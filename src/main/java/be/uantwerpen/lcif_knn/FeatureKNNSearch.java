package be.uantwerpen.lcif_knn;

import java.util.HashSet;
import java.util.Set;

import be.uantwerpen.util.Timer;
import be.uantwerpen.util.model.ListMap;
import be.uantwerpen.util.model.MyMinHeap;
import be.uantwerpen.util.model.Pair;

import com.zaxxer.sparsebits.SparseBitSet;

public class FeatureKNNSearch {

	private SparseBitSet[] features;
	private SparseBitSet[] labels;
	
	public  FeatureKNNCache computeAllFeatureLabelSimmilarities(LCIFData data, int maxK){
		Timer timer = new Timer("item-item matrix");
		//keep all sparse vectors in memory
		features = makeSparseBitsetsFeatures(data);
		labels = makeSparseBitsetsLabels(data);
		timer.progress("made sparse bitsets",0);
		ListMap<Integer,FeatureNeighbor> simils = new ListMap<>();
		for(int featureIdx=0; featureIdx<data.numberOfFeaturesInTraining(); featureIdx++){
			if(featureIdx % 100000 == 0){
				timer.progress(featureIdx, data.numberOfFeaturesInTraining());
			}
			MyMinHeap<Double, Integer> neighbours = computeSimilarLabelsTopK(data, featureIdx, maxK);
			for(Pair<Double,Integer> pair: neighbours.values()){
				 simils.put(featureIdx, new FeatureNeighbor(pair.getSecond(), pair.getFirst()));
			}
		}
		timer.end();
		FeatureKNNCache cache = new FeatureKNNCache();
		cache.setCache(simils);
		features = labels = null; //may be garbage collected
		System.gc();
		return cache;
	}
	
	public  MyMinHeap<Double, Integer> computeSimilarLabelsTopK(LCIFData data, int feature_id, int maxK){
		//fetch possible labels 
		int[] rows = data.getTrainingRowsHaving(feature_id);
		Set<Integer> labelsSharingOneTrainingExample = new HashSet<Integer>();
		for(int row: rows){
			for(int label: data.getTrainingRowLabels(row)){
				labelsSharingOneTrainingExample.add(label);
			}
		}
		MyMinHeap<Double, Integer> myMinHeap = new MyMinHeap<Double, Integer>(maxK);
		//check each label
		for(int label: labelsSharingOneTrainingExample){
			SparseBitSet s1 = features[feature_id];
			SparseBitSet s2 = labels[label];
			double simil = cosineSimmilarity(s1, s2);
			if(simil > 0.000001)
				myMinHeap.add(simil, label);
		}
		return myMinHeap;
	}	
	
	private SparseBitSet[] makeSparseBitsetsFeatures(LCIFData data){
		SparseBitSet[] features = new SparseBitSet[data.numberOfFeaturesInTraining()];
		for(int feature_id=0; feature_id<data.numberOfFeaturesInTraining(); feature_id++){
			int[] rows = data.getTrainingRowsHaving(feature_id);
			SparseBitSet s1 = new SparseBitSet();
			for(int i=0; i<rows.length;i++){
				s1.set(rows[i]);
			}
			features[feature_id] = s1;
		}
		return features;
	}
	
	private SparseBitSet[] makeSparseBitsetsLabels(LCIFData data){
		SparseBitSet[] labels = new SparseBitSet[data.numberOfLabelsInTraining()];
		int noLabels = data.numberOfLabelsInTraining();
		for(int label_id=0; label_id<noLabels; label_id++){
			SparseBitSet s2 = new SparseBitSet();
			int[] rows_labels = data.getTrainingRowsHavingLabel(label_id);
			if(rows_labels != null){
				for(int i=0; i<rows_labels.length;i++){
					s2.set(rows_labels[i]);
				}
			}
			labels[label_id] = s2;
		}
		return labels;
	}
		
	private  double cosineSimmilarity(SparseBitSet r1, SparseBitSet r2){
		double denominator = RowKNNSearch.norm(r1.cardinality()) * RowKNNSearch.norm(r2.cardinality()); 
		if(r1.cardinality() == 0 || r2.cardinality() == 0)
			return 0.0;
		int intersectionSize = SparseBitSet.and(r1, r2).cardinality();
		if(intersectionSize == 0)
			return 0.0;
		return intersectionSize / denominator; 
	}

}
