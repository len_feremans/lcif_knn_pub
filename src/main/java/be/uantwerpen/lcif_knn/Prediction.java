package be.uantwerpen.lcif_knn;

public class Prediction {
	
	private int labelId;
	private double score;
	
	public Prediction(int labelId, double score) {
		super();
		this.labelId = labelId;
		this.score = score;
	}
	
	public int getLabelId() {
		return labelId;
	}
	//public void setLabelId(int labelId) {
	//	this.labelId = labelId;
	//}
	public double getScore() {
		return score;
	}
	//public void setScore(double score) {
	//	this.score = score;
	//}
	
	public String toString(){
		return String.format("%d:%.3f", labelId,score);
	}
	
}
