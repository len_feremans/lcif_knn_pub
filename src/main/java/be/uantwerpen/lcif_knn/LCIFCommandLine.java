package be.uantwerpen.lcif_knn;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

import be.uantwerpen.lcif_knn.LCIFEvaluation.EvaluationMetricValue;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.util.CollectionUtils;

public class LCIFCommandLine {

	RowKNNSearch search = new RowKNNSearch();
	RowKNNPredictions predictions = new RowKNNPredictions();
	LCIFEvaluation evaluator = new LCIFEvaluation();

	FeatureKNNSearch searchFeatures = new FeatureKNNSearch();
	FeatureKNNPredictions predictionsFeatures = new FeatureKNNPredictions();

	SecondRowKNNSearch searchSecondRow = new SecondRowKNNSearch();
	SecondRowKNNPredictions predictionsSecondRow = new SecondRowKNNPredictions();
	
	LCIFLinearCombination linearCombination = new LCIFLinearCombination();
	//e.g. LCIFCommandLine medical-train.libsvm  medical-test.libsvm instance-knn -k1 10 -t 0.0
	//e.g. LCIFCommandLine medical-train.libsvm  medical-test.libsvm feature-knn -k1 5 -t 0.0
	//e.g. LCIFCommandLine medical-train.libsvm  medical-test.libsvm second-instance-knn -k1 5 -k2 10 -t 0.0
	//e.g. LCIFCommandLine medical-train.libsvm  medical-test.libsvm lcif -k1 10 -k2 5 -l 0.6 -t 0.0
	//e.g. LCIFCommandLine medical-train.libsvm  medical-test.libsvm lcif2 -k1 10 -k2 10 -t 0.0 -l 0.6 -v 
	//saves one line to all_results_lcif.csv
	
	public static void main(String[] args) throws Exception{
		String train = args[0];
		String test = args[1];
		String method = args[2];
		Integer k1 = null;
		Integer k2 = null;
		Double lambda = null;
		Double threshold = null;
		boolean debug = false;
		File output = null;
		for(int i=3; i<args.length; i+=2){
			if(args[i].equals("-k1")){
				k1 = Integer.valueOf(args[i+1]);
			}
			else if(args[i].equals("-k2")){
				k2 = Integer.valueOf(args[i+1]);
			}
			else if(args[i].equals("-l")){
				lambda = Double.valueOf(args[i+1]);
			}
			else if(args[i].equals("-t")){
				threshold = Double.valueOf(args[i+1]);
			}
			else if(args[i].equals("-o")){ //new
				output = new File(args[i+1]);
			}
			else if(args[i].equals("-v")){
				debug = true;
				i--;
			}
			else
				throw new RuntimeException("Unknown parameter " + args[i]);
		}
		File trainLibsvm = new File(train);
		File testLibsvm = new File(test);
		new LCIFCommandLine().runLCIF(trainLibsvm,testLibsvm, method, k1, k2, threshold, lambda, output, debug);
	}
	
		

	public void runLCIF(File trainLibsvm, File testLibsvm, String method, Integer k1, Integer k2, Double threshold, Double lambda, File outputPredictions, boolean debug) throws InterruptedException, IOException{
		String caller = String.format("LCIFCommandLine;dataset=%s;method=%s;k1=%d;k2=%d;lambda=%.3f;threshold=%.3f;", trainLibsvm, method, k1, k2, lambda, threshold);
		Timer timer = new Timer(caller);
		Timer.VERBOSE = debug;

		LCIFData data = new LCIFData(trainLibsvm, testLibsvm);
		//row knn
		RowKNNCache cache = null;
		LCIFPredictions scoresRow = null;
		if(ArrayUtils.contains(new String[]{"instance-knn","second-instance-knn","lcif","lcif2"}, method)){
			cache = search.computeAllSimilarRowsMultiThreaded(data, k1);
			if(method.equals("second-instance-knn")){
				;//no scores needed...
			}
			else{
				scoresRow = predictions.makePredictionAllTestRows(data, cache, k1);
				List<EvaluationMetricValue> evaluation = evaluator.computeMetricValues(data, scoresRow.doOneThreshold(threshold));
				long elapsed = timer.elapsed();
				System.out.println("ELAPSED ROW KNN " + k1 + ":" + elapsed);
				System.out.println(CollectionUtils.join(evaluation));
			}
		}
		//feature knn
		LCIFPredictions scores2 = null;
		if(ArrayUtils.contains(new String[]{"feature-knn", "lcif"}, method)){
			int kFeature = k1;
			if(method.equals("lcif"))
				kFeature = k2;
			FeatureKNNCache cacheF = searchFeatures.computeAllFeatureLabelSimmilarities(data, kFeature);
			scores2 = predictionsFeatures.makePredictionAllTestRows(data, cacheF, kFeature);
			List<EvaluationMetricValue> evaluation = evaluator.computeMetricValues(data, scores2.doOneThreshold(threshold));
			long elapsed = timer.elapsed();
			System.out.println("ELAPSED FEATURE KNN " + kFeature + ":" +  elapsed);
			System.out.println(CollectionUtils.join(evaluation));			
		}
		//second-row knn
		if(ArrayUtils.contains(new String[]{"second-instance-knn","lcif2"}, method)){
			SecondRowKNNCache cache2 = searchSecondRow.computeAllSimilarRows(data, k2);
			scores2 = predictionsSecondRow.makePredictionAllTestRows(data, cache, cache2, k1, k2);
			List<EvaluationMetricValue> evaluation = evaluator.computeMetricValues(data, scores2.doOneThreshold(threshold));
			long elapsed = timer.elapsed();
			System.out.println("ELAPSED SECOND ROW KNN " + k1 + "," + k2 + ":" + elapsed);
			System.out.println(CollectionUtils.join(evaluation));
		}
		//LCIF1/2
		if(ArrayUtils.contains(new String[]{"lcif","lcif2"}, method)){
			//LCIFPredictions predictionsCombo = linearCombination.makePredictionAllTestRows(data, scoresRow, scores2, lambda);
			LCIFPredictions predictionsCombo = linearCombination.makePredictionAllTestRows(data, scoresRow, scores2, lambda, outputPredictions);
			predictionsCombo.load(outputPredictions);
			LCIFPredictions predictions = predictionsCombo.doOneThreshold(threshold);
			List<EvaluationMetricValue> evaluation = evaluator.computeMetricValues(data, predictions);
			long elapsed = timer.elapsed();
			System.out.println("ELAPSED LCIF" + (method.equals("lcif2")?"2 ":" ") +  k1 + "," + k2 + "," + lambda + ":" + elapsed);
			System.out.println(CollectionUtils.join(evaluation));
		}
		//save runtimes
		long elapsed = timer.elapsed();
		File runtimesLogs = new File("./temp/lcif-performance.log");
		FileWriter writer = new FileWriter(runtimesLogs, true);
		writer.write(String.format("%s%d\n", caller,elapsed));
		writer.close();
	}

}
