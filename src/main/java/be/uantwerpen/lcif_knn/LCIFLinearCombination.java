package be.uantwerpen.lcif_knn;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class LCIFLinearCombination {

	public LCIFPredictions makePredictionAllTestRows(LCIFData data, LCIFPredictions predictions1, LCIFPredictions predictions2, double lambda){
		try{
			return makePredictionAllTestRows(data, predictions1, predictions2, lambda, null);
		}catch(IOException e){
			throw new RuntimeException(e);
		}
	}
	
	public LCIFPredictions makePredictionAllTestRows(LCIFData data, LCIFPredictions predictions1, LCIFPredictions predictions2, double lambda, File outputPredictionsFile) throws IOException {
		LCIFPredictions predictions = new LCIFPredictions();
		BufferedWriter writer = null;
		if(outputPredictionsFile != null)
			if(!outputPredictionsFile.getParentFile().exists()) {
				outputPredictionsFile.getParentFile().mkdirs();
			}
			writer = new BufferedWriter(new FileWriter(outputPredictionsFile));
		for(int i=0; i<data.numberOfRowsInTest(); i++){
			List<Prediction> predictionsInstanceRow = predictions1.getPredictionsRaw(i);
			List<Prediction> predictionsFeatureRow = predictions2.getPredictionsRaw(i);
			if(!Double.isNaN(predictions1.getThreshold()) ||!Double.isNaN(predictions2.getThreshold()))
				throw new RuntimeException("Should not get here");
			List<Prediction> combinedPredictions = makePredictionRow(predictionsInstanceRow, predictionsFeatureRow, lambda);
			if(writer != null)
				RowKNNPredictions.savePrediction(writer, i, combinedPredictions);
			else{
				predictions.add(combinedPredictions);
			}
		}
		if(writer != null){
			writer.close();
			System.out.println("Saved " + outputPredictionsFile.getAbsolutePath());
		}
		return predictions;
	}
	

	public  List<Prediction> makePredictionRow(List<Prediction> predictions1, List<Prediction> predictions2, double lambda){
		//Special cases als lambda == 0.0, e.g. alle labels van user-based die niet voorkomen in item-based, krijgen toch nog 0.0...
		if(lambda == 0.0){
			return predictions2;
		}
		else if(lambda == 1.0){
			return predictions1;
		}
		Map<Integer, Double> linearCombo = new HashMap<Integer,Double>();
		for(Prediction p1: predictions1){
			linearCombo.put(p1.getLabelId(), p1.getScore() * lambda); //e.g. for each label: prediction = predictionRow * lambda
		}
		for(Prediction p2: predictions2){
			Double score = linearCombo.get(p2.getLabelId());
			score = (score == null)?0.0:score;
			score += (1 - lambda) * p2.getScore();  //e.g. for each label: prediction = prectionFeature * (1-lambda) 
												   //.... + predictionRow * lambda (which can be 0.0)
			linearCombo.put(p2.getLabelId(), score);
		}
		List<Prediction> predictionsTarget = new ArrayList<Prediction>();
		for(Entry<Integer,Double> entry: linearCombo.entrySet()){
			predictionsTarget.add(new Prediction(entry.getKey(), entry.getValue()));
		}
		Collections.sort(predictionsTarget, RowKNNPredictions.highToLow);
		return predictionsTarget;
	}
}
