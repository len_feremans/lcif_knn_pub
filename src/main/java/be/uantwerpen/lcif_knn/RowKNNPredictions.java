package be.uantwerpen.lcif_knn;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import be.uantwerpen.util.Timer;

public class RowKNNPredictions {
	
	public LCIFPredictions makePredictionAllTestRows(LCIFData data, RowKNNCache cache, int k){
		try{
			File file = new File("./temp/" + data.getDatasetName() + "-predictions-row-knn-k=" + k + ".txt");
			if(!file.getParentFile().exists())
				file.getParentFile().mkdirs();
			makePredictionAllTestRows(data,cache,k,file);
			LCIFPredictions result = new LCIFPredictions();
			result.load(file);
			return result;
		}catch(IOException e){
			throw new RuntimeException(e);
		}
	}
	public  void makePredictionAllTestRows(LCIFData data, RowKNNCache cache, int k, File outputPredictionsFile) throws IOException {
		Timer timer = new Timer("makePredictionsRow");
		List<Prediction> preferences = new ArrayList<>();
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputPredictionsFile));
		//predictions user-based
		for(int test_row=0; test_row<data.numberOfRowsInTest(); test_row++){
			if(test_row % 2000 == 0){
				timer.progress(test_row, data.numberOfRowsInTest());
			}
			preferences = makePredictionRow(data, cache, test_row, k);
			savePrediction(writer, test_row, preferences);
		}
		writer.close();
		System.out.println("Saved " + outputPredictionsFile.getAbsolutePath());
		timer.end();
	}
	
	public  List<Prediction> makePredictionRow(LCIFData data, RowKNNCache cache, int test_id, int k) 
	{
		List<Prediction> preferences = new ArrayList<>();
		//neighbours from cache
		List<Neighbor> neighbors = cache.getSimmilarRowsTopK(data, test_id, k);
		//compute
		Map<Integer,Double> sums = new HashMap<Integer,Double>();
		for(Neighbor nb: neighbors){
			int[] labels = data.getTrainingRowLabels(nb.getTrainingRowId());
			for(Integer label: labels){
				Double sum = sums.get(label);
				if(sum == null){
					sum = 0.0;
				}
				sums.put(label, sum + nb.getSimilarity());
			}
		}
		//normalize
		double denominator = 0.0;      //correct? Same normalisation for all labels... Yes
		for(Neighbor nb: neighbors){
			denominator += nb.getSimilarity();
		}
		for(Entry<Integer,Double> labelSum: sums.entrySet()){
			preferences.add(new Prediction(labelSum.getKey(), labelSum.getValue()/denominator));
		}
		
		//sort
		Collections.sort(preferences, highToLow);
		//preferences = new ArrayList<>(preferences.subList(0, Math.min(maxPredictions,preferences.size())));
		//if(preferences.size() == 0){
		//	System.err.println("no preferences");
		//}
		return preferences;
	}		
	
	public static Comparator<Prediction> highToLow = new Comparator<Prediction>() {

		@Override
		public int compare(Prediction o1, Prediction o2) {
			return new Double(o2.getScore()).compareTo(new Double(o1.getScore()));
		}
	};
	
	//Note: copy paste with save neighbours
	public static  void savePrediction(BufferedWriter writer, int test_row, List<Prediction> predictions)
			throws IOException {
		writer.write(String.format("%d", test_row));
		writer.write(",");
		for(Prediction pred: predictions){
			writer.write(String.format("%d",pred.getLabelId()));
			writer.write(":");
			writer.write(String.format("%.12f",pred.getScore()));
			writer.write(",");
		}
		writer.write("\n");
	}
	
}
