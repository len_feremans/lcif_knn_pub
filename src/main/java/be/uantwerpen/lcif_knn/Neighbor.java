package be.uantwerpen.lcif_knn;

public class Neighbor {
	
	private int trainingRowId;
	private double similarity;
	
	public Neighbor(int trainingRowId, double similarity) {
		super();
		this.trainingRowId = trainingRowId;
		this.similarity = similarity;
	}
	
	public int getTrainingRowId() {
		return trainingRowId;
	}
	public double getSimilarity() {
		return similarity;
	}
	
	
}
