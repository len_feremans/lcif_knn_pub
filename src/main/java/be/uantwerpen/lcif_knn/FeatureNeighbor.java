package be.uantwerpen.lcif_knn;

public class FeatureNeighbor {
	
	private int labelId;
	private double similarity;
	
	public FeatureNeighbor(int labelId, double similarity) {
		super();
		this.labelId = labelId;
		this.similarity = similarity;
	}
	
	public int getLabelId() {
		return labelId;
	}
	public double getSimilarity() {
		return similarity;
	}
	
	public String toString(){
		return String.format("%d:%.3f", labelId, similarity);
	}
}
