package be.uantwerpen.lcif_knn;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import be.uantwerpen.util.Timer;
import be.uantwerpen.util.model.CountMap;
import be.uantwerpen.util.model.MyMinHeap;
import be.uantwerpen.util.model.Pair;
import be.uantwerpen.util.util.IOUtils;
import be.uantwerpen.util.util.IOUtils.Streamer2;


public class RowKNNSearch {

	//COMPUTE Norms +-fast
	final public static double norm(int x){
		if(x > noSquareRoots){
			return Math.sqrt(x);
		}
		if(squareRoots[1] == 0.0)
			computeSquareRoots();
		return squareRoots[x];
	}
	
	private static final int noSquareRoots = 50000;
	private static double[] squareRoots = new double[noSquareRoots];
	private static void computeSquareRoots(){
		for(int i=0; i<noSquareRoots; i++){
			squareRoots[i] = Math.sqrt(i);
		}
	}	
	
	//compute closest neighbor for all test instances
	public RowKNNCache computeAllSimilarRows(LCIFData data, final int k){
		try {
			File file = File.createTempFile("similar-knn", "cache");
			computeAllSimilarRows(data,k,file);
			RowKNNCache cache = new RowKNNCache();
			cache.load(file);
			return cache;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public RowKNNCache computeAllSimilarRowsMultiThreaded(LCIFData data, final int k) throws InterruptedException{
		try {
			int threads = 50; // FOR HPC --> CHANGE!
			List<File> files = new ArrayList<>();
			for(int i=0; i<threads;i++){
				File file = File.createTempFile("similar-knn", "cache");
				files.add(file);
			}
			int partitionSize = data.numberOfRowsInTest()/threads;
			List<Thread> threadObjects = new ArrayList<>();
			for(int i=0; i<threads;i++){
				final File currentOutput = files.get(i);
				final int start = i * partitionSize;
				final int end =  start + partitionSize; //OK: Be secure here
				final int rend = (i==threads-1)?  data.numberOfRowsInTest(): end;
				Thread t = new Thread(){
					@Override
					public void run() {
						try {
							computeAllSimilarRows(data,k,start,rend,currentOutput);
						} catch (IOException e) {
							throw new RuntimeException(e);
						}
					}
				};
				t.start();
				threadObjects.add(t);
			}
			for(Thread threadObject: threadObjects){
				threadObject.join();
			}
			File file = File.createTempFile("all-similar-knn", "cache");
			for(File partialOutput: files){
				IOUtils.stream2(partialOutput, file, new Streamer2() {
					
					@Override
					public String doLine(String line) {
						return line;
					}
				}, true);
			}
			RowKNNCache cache = new RowKNNCache();
			cache.load(file);
			return cache;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	void computeAllSimilarRows(LCIFData data, final int k, final File neigbhorFile) throws IOException {
		computeAllSimilarRows(data, k, 0, data.numberOfRowsInTest(), neigbhorFile);
	}
	
	void computeAllSimilarRows(LCIFData data, final int k, int startTest, int endTest, final File neigbhorFile) throws IOException {
		//neighbours computation, multi-threaded
		//How? each thread handles 1/#no-threads of the computations, interleaved, and saves to file, then join all files...
		Timer timer = new Timer("computeAllSimilarRows  " + startTest + "-" + endTest);	
		long total = 0;
		BufferedWriter writer  = new BufferedWriter(new FileWriter(neigbhorFile));
		for(int test_row=startTest; test_row<endTest; test_row+=1){
			if(test_row % 500 == 0)
				timer.progress(test_row - startTest, endTest - startTest);
			MyMinHeap<Double, Integer> neighbours = computeSimmilarRowsTopK(data, test_row, k);
			if(neighbours.size() == 0){
				System.err.println("Warning: no neighbors for test_id");
			}
			total += neighbours.size();
			saveNeighboursRow(writer, test_row, neighbours.values()); //sorted
		}
		writer.close();
		System.out.println("Saved " + neigbhorFile);
		timer.end();
		System.out.println("Saved #neighbours: " + total + ", for test instances: " + startTest + " to " + endTest + 
				" avg number of neighbours per instance: " + total / (double)(endTest - startTest));
	}
	
	//Uses cosine similarity of binary vector 
	public  MyMinHeap<Double, Integer> computeSimmilarRowsTopK(LCIFData data, int test_id, int maxK){
		int[] testrow_features = data.getTestRowFeatures(test_id);
		double normTest = norm(testrow_features.length);
		CountMap<Integer> rowCounts = new CountMap<>();
		for(int test_feature: testrow_features){
			int[] rows = data.getTrainingRowsHaving(test_feature);
			for(int row: rows){
				rowCounts.add(row);
			}
		}
		MyMinHeap<Double, Integer> myMinHeap = new MyMinHeap<Double, Integer>(maxK);
		for(Entry<Integer,Integer> entry: rowCounts.getMap().entrySet()){
			int rowId = entry.getKey();
			int featuresInCommon =  entry.getValue();
			double normTraining = norm(data.getTrainingRowFeatures(rowId).length);
			double cosineSimillarity = featuresInCommon / (normTraining  * normTest);
			if(cosineSimillarity > 0.000001)
				myMinHeap.add(cosineSimillarity, entry.getKey());
		}
		return myMinHeap;
	}	
	
	
	public static void saveNeighboursRow(BufferedWriter writer, int row, List<Pair<Double,Integer>> preferences)
			throws IOException {
		writer.write(String.format("%d", row));
		writer.write(",");
		for(Pair<Double,Integer> pair: preferences){
			writer.write(String.format("%d",pair.getSecond()));
			writer.write(":");
			writer.write(String.format("%.12f",pair.getFirst()));
			writer.write(",");
		}
		writer.write("\n");
	}
	
}
