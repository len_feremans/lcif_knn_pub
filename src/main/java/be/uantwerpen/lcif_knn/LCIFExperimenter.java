package be.uantwerpen.lcif_knn;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import be.uantwerpen.lcif_knn.LCIFEvaluation.EvaluationMetricValue;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.model.Pair;
import be.uantwerpen.util.util.IOUtils;
import be.uantwerpen.util.util.MathUtils;

public class LCIFExperimenter {
	
	//singletons:
	RowKNNSearch rowKNNSearch = new RowKNNSearch();
	RowKNNPredictions rowKNNPredictions = new RowKNNPredictions();
	FeatureKNNSearch featureKNNSearch = new FeatureKNNSearch();
	FeatureKNNPredictions featureKNNPredictions = new FeatureKNNPredictions(); 
	SecondRowKNNSearch secondRowKNNSearch = new SecondRowKNNSearch();
	SecondRowKNNPredictions secondRowKNNPredictions = new SecondRowKNNPredictions();
	LCIFLinearCombination lcifLinearCombination = new LCIFLinearCombination();
	LCIFEvaluation evaluator = new LCIFEvaluation();

	boolean LCIF2_MODUS = false;

	public void setUseNoLabelPredictHighesHeuristic(){
		evaluator.setUseNoLabelPredictHighesHeuristic();
	}

	public void setLCIF2Modus(){
		this.LCIF2_MODUS = true;
	}

	public static class LCIFResult{
		public int kRow;
		public int kFeature;
		public double lambda;
		public double optimalTreshold;
		public List<EvaluationMetricValue> evaluationValues;
		public LCIFResult(int kRow, int kFeature, double lambda,
				double optimalTreshold,
				List<EvaluationMetricValue> evaluationValues) {
			super();
			this.kRow = kRow;
			this.kFeature = kFeature;
			this.lambda = lambda;
			this.optimalTreshold = optimalTreshold;
			this.evaluationValues = evaluationValues;
		}

		public double getEvaluationMetricValue(String s){
			for(EvaluationMetricValue val: evaluationValues){
				if(val.name.equals(s))
					return val.value;
			}
			throw new RuntimeException("here");
		}

		public String toString(){
			return String.format("LCIFResult(kRow=%d,kFeature=%d,lambda=%.3f,t=%.3f,metric=%s)", kRow,kFeature,lambda,optimalTreshold,evaluationValues);
		}

		public String toCSVLine(String method){
			return String.format("method=%s;kRow=%d;kFeature=%d;threshold=%.3f;lambda=%.3f;microF1=%.3f;macroF1=%.3f;F1=%.3f;acc=%.3f;hloss=%.3f\n", 
					method,
					kRow, kFeature, optimalTreshold, lambda,
					getEvaluationMetricValue(EvaluationMetricValue.MICRO_F1),
					getEvaluationMetricValue(EvaluationMetricValue.MACRO_F1),
					getEvaluationMetricValue(EvaluationMetricValue.F1),
					getEvaluationMetricValue(EvaluationMetricValue.ACCURACY),
					getEvaluationMetricValue(EvaluationMetricValue.HAMMING_LOSS));
		}

		public String toCSVLineBest(String method, String metric){
			return String.format("method=%s;mode=gridsearch;kRow=%d;kFeature=%d;threshold=%.3f;lambda=%.3f;%s=%.3f\n",
					method,
					kRow, kFeature, optimalTreshold, lambda,
					metric, getEvaluationMetricValue(metric));
		}
	}

	//e.g. returns
	//kRow=1,kFeature=1,lambda=1,t=0.0,microF1=...,macroF1=...,acc=...,f1=..,hamminglos=...
	public List<LCIFResult> runLCIFGridSearchSingleFold(File training, File test, int[] kRowRange, int[] kFeatureRange, double[] lambdaRange) throws Exception{
		//do row-knn
		//10-fold grid search on training part
		//vary kR between 1 and 30 for rowKnn
		//vary kF between 1 and 30 for featureKNN
		//optimize for each k t based on label cardinality
		System.out.println("Gridsearch");
		int maxKRow = MathUtils.maxInt(kRowRange);
		int maxKFeature = MathUtils.maxInt(kFeatureRange);
		//results will have 30 * 30 * 20 values = 18000 values
		//init data
		LCIFData data = new LCIFData(training,test);
		data.printDataStats();
		HashMap<Integer,LCIFPredictions> rowBasedPredictions = new HashMap<>();
		HashMap<Integer,LCIFPredictions> featureBasedPredictions = new HashMap<>();
		HashMap<String,LCIFPredictions> secondInstanceBasedPredictions = new HashMap<>();
		if(!LCIF2_MODUS){
			//run predictions instance based
			RowKNNCache cache = rowKNNSearch.computeAllSimilarRowsMultiThreaded(data, maxKRow);
			for(int kRow: kRowRange){
				LCIFPredictions predictionsData = rowKNNPredictions.makePredictionAllTestRows(data, cache, kRow);
				rowBasedPredictions.put(kRow,predictionsData);
			}
			//run predictions feature based			
			FeatureKNNCache cacheFeatures = featureKNNSearch.computeAllFeatureLabelSimmilarities(data, maxKFeature);
			for(int kFeature: kFeatureRange){
				LCIFPredictions predictionsData = featureKNNPredictions.makePredictionAllTestRows(data, cacheFeatures, kFeature);
				featureBasedPredictions.put(kFeature,predictionsData);
			}
		}
		else{
			//second instance variation
			RowKNNCache cache = rowKNNSearch.computeAllSimilarRowsMultiThreaded(data, maxKRow);
			SecondRowKNNCache cacheSecondInstance = secondRowKNNSearch.computeAllSimilarRows(data, maxKFeature); 
			for(int kRow: kRowRange){
				for(int kFeature: kFeatureRange){
					LCIFPredictions predictionsData = secondRowKNNPredictions.makePredictionAllTestRows(data, cache, cacheSecondInstance,kRow, kFeature);
					secondInstanceBasedPredictions.put(kRow + "-" + kFeature,predictionsData);
				}
			}
			//run predictions feature based			
			FeatureKNNCache cacheFeatures = featureKNNSearch.computeAllFeatureLabelSimmilarities(data, maxKFeature);
			for(int kFeature: kFeatureRange){
				LCIFPredictions predictionsData = featureKNNPredictions.makePredictionAllTestRows(data, cacheFeatures, kFeature);
				featureBasedPredictions.put(kFeature,predictionsData);
			}
		}

		//run linear combination
		Timer gridsearch = new Timer("gridsearch LCIF");
		List<LCIFResult> results = new ArrayList<>();
		int runs = 0;
		for(int kRow: kRowRange){
			for(int kFeature: kFeatureRange){
				LCIFPredictions rowPredictions = null;
				LCIFPredictions featurePredictions  = null;
				if(!LCIF2_MODUS){
					rowPredictions = rowBasedPredictions.get(kRow);
					featurePredictions = featureBasedPredictions.get(kFeature);
				}
				else{
					rowPredictions = secondInstanceBasedPredictions.get(kRow+"-"+kFeature);
					featurePredictions = featureBasedPredictions.get(kFeature);
				}
				for(double lambda: lambdaRange){
					runs++;
					if(runs % 10 == 0)
						gridsearch.progress(runs, kRowRange.length * kFeatureRange.length * lambdaRange.length);
					//Linear combination
					LCIFPredictions predictionsCombo = lcifLinearCombination.makePredictionAllTestRows(data, rowPredictions, featurePredictions, lambda);
					//one threshold based on label cardinality
					double optimalThreshold = evaluator.computeOptimalSingleThresholdLabelCardinality(data, predictionsCombo); 
					//evaluate
					List<EvaluationMetricValue> values = evaluator.computeMetricValues(data, predictionsCombo.doOneThreshold(optimalThreshold));
					results.add(new LCIFResult(kRow, kFeature, lambda, optimalThreshold, values));
				}
			}
		}
		gridsearch.end();
		return results;
	}

	public List<LCIFResult> runLCIFGridSearchFolded(File training, int folds,  int[] kRowRange, int[] kFeatureRange, double[] lambdaRange) throws Exception{
		List<Pair<File,File>> pairs = IOUtils.randomFolds(training, folds);
		List<List<LCIFResult>> nResults = new ArrayList<>();
		for(int i=0; i<folds;i++){
			System.out.println("========= FOLD " + i + " ===========");
			File train = pairs.get(i).getFirst();
			File test = pairs.get(i).getSecond();
			List<LCIFResult> foldIResults = runLCIFGridSearchSingleFold(train,test, kRowRange, kFeatureRange, lambdaRange);
			nResults.add(foldIResults);
		}
		//take average evaluation metrics
		List<LCIFResult> averaged = new ArrayList<LCIFResult>();
		for(int configuration=0; configuration < nResults.get(0).size(); configuration++){
			int kRow = nResults.get(0).get(configuration).kRow;
			int kFeature = nResults.get(0).get(configuration).kFeature;
			double lambda = nResults.get(0).get(configuration).lambda;
			List<LCIFResult> resultsSameConf = new ArrayList<>();
			for(int i=0; i<folds;i++){
				resultsSameConf.add(nResults.get(i).get(configuration));
			}
			double averageOptimalTreshold = resultsSameConf.stream().mapToDouble((res) -> res.optimalTreshold).sum() / folds;
			double averageMicroF1 = resultsSameConf.stream().mapToDouble((res) -> res.getEvaluationMetricValue(EvaluationMetricValue.MICRO_F1)).sum() / folds;
			double averageMacroF1 = resultsSameConf.stream().mapToDouble((res) -> res.getEvaluationMetricValue(EvaluationMetricValue.MACRO_F1)).sum() / folds;
			double averageF1 = resultsSameConf.stream().mapToDouble((res) -> res.getEvaluationMetricValue(EvaluationMetricValue.F1)).sum() / folds;
			double averageAcc = resultsSameConf.stream().mapToDouble((res) -> res.getEvaluationMetricValue(EvaluationMetricValue.ACCURACY)).sum() / folds;
			double averageHammingLoss = resultsSameConf.stream().mapToDouble((res) -> res.getEvaluationMetricValue(EvaluationMetricValue.HAMMING_LOSS)).sum() / folds;
			List<EvaluationMetricValue> averageEvaluations = Arrays.asList(new EvaluationMetricValue[]{
					new EvaluationMetricValue(EvaluationMetricValue.MICRO_F1, averageMicroF1),
					new EvaluationMetricValue(EvaluationMetricValue.MACRO_F1, averageMacroF1),
					new EvaluationMetricValue(EvaluationMetricValue.F1, averageF1),
					new EvaluationMetricValue(EvaluationMetricValue.ACCURACY, averageAcc),
					new EvaluationMetricValue(EvaluationMetricValue.HAMMING_LOSS, averageHammingLoss)
			});
			LCIFResult averagedResult = new LCIFResult(kRow, kFeature, lambda, averageOptimalTreshold, averageEvaluations);
			averaged.add(averagedResult);
		}
		return averaged;
	}

	public void reportParameters(File training, File test, List<LCIFResult> gridsearchResults, File output) throws Exception{
		FileWriter writer = new FileWriter(output);
		//best for row-knn: configuration where lambda = 1.0 (and kItem=1 since that doesn't matter if lambda is 1.0)
		List<LCIFResult> prunedResultsRowKnn = gridsearchResults.stream().filter((res) -> res.kFeature == 1 && res.lambda == 1.0).collect(Collectors.toList());
		if(LCIF2_MODUS){
			prunedResultsRowKnn = gridsearchResults.stream().filter((res) -> res.lambda == 1.0).collect(Collectors.toList());
		}
		System.out.println("=== Gridsearch row-knn ===");
		for(LCIFResult res: prunedResultsRowKnn){
			String method = !LCIF2_MODUS?"row-knn":"second-knn";
			System.out.print(res.toCSVLine(method));
		}
		//best:
		for(String metric: new String[]{
				EvaluationMetricValue.MICRO_F1, EvaluationMetricValue.MACRO_F1, 
				EvaluationMetricValue.F1, EvaluationMetricValue.ACCURACY, EvaluationMetricValue.HAMMING_LOSS}){
			LCIFResult optimalParameter = bestResult(prunedResultsRowKnn, metric);
			String method = !LCIF2_MODUS?"row-knn":"second-knn";
			String outputStr = optimalParameter.toCSVLineBest(method, metric);
			System.out.print(outputStr);
			writer.write(outputStr);
		}
		//best for feature-knn: configuration where lambda = 0.0 (and kUser=1 since that doesn't matter if lambda is 0.0) 
		//for second-instance (LCIF2_MODUS): kUser varying is important
		List<LCIFResult> prunedResultsFeatureKnn = gridsearchResults.stream().filter((res) -> res.kRow ==1 && res.lambda == 0.0).collect(Collectors.toList());
		System.out.println("=== Gridsearch feature-knn ===");
		for(LCIFResult res: prunedResultsFeatureKnn){
			System.out.print(res.toCSVLine("feature-knn"));
		}
		//best:
		for(String metric: new String[]{ EvaluationMetricValue.MICRO_F1, EvaluationMetricValue.MACRO_F1, EvaluationMetricValue.F1, 
				EvaluationMetricValue.ACCURACY, EvaluationMetricValue.HAMMING_LOSS}){
			LCIFResult optimalParameter = bestResult(prunedResultsFeatureKnn, metric);
			String outputStr = optimalParameter.toCSVLineBest("feature-knn", metric);
			System.out.print(outputStr);
			writer.write(outputStr);
		}
		//best LCIF
		System.out.println("=== Gridsearch LCIF  ===");
		//best:
		for(String metric: new String[]{ EvaluationMetricValue.MICRO_F1, EvaluationMetricValue.MACRO_F1,  EvaluationMetricValue.F1, 
				EvaluationMetricValue.ACCURACY, EvaluationMetricValue.HAMMING_LOSS}){
			LCIFResult optimalParameter = bestResult(gridsearchResults, metric);
			String method = !LCIF2_MODUS?"lcif-knn":"lcif2-knn";
			String outputStr = optimalParameter.toCSVLineBest(method, metric);
			System.out.print(outputStr);
			writer.write(outputStr);
		}
		writer.close();
	}

	private LCIFResult bestResult(List<LCIFResult> results, String metric){
		boolean higherBetter = metric.equals(EvaluationMetricValue.HAMMING_LOSS)? false: true;
		LCIFResult optimalParameter = results.stream().max((res1,res2) -> {
			Double first = new Double(res1.getEvaluationMetricValue(metric));
			Double second = new Double(res2.getEvaluationMetricValue(metric));
			if(higherBetter){
				return first.compareTo(second);
			}
			else{
				return second.compareTo(first);
			}
		}).get();
		return optimalParameter;
	}



	public List<LCIFResult> parseResults(File input) throws Exception{
		List<String> lines = IOUtils.readFile(input);
		List<LCIFResult> results = new ArrayList<>();
		for(String line: lines){
			Map<String,String> parsed = parseLine(line);
			String method = parsed.get("method");
			int kRow = Integer.valueOf(parsed.get("kRow"));
			int kFeature = Integer.valueOf(parsed.get("kFeature"));
			double t = Double.valueOf(parsed.get("threshold"));
			double lambda = Double.valueOf(parsed.get("lambda"));
			String metric = parsed.get("last");
			LCIFResult r = new LCIFResult(kRow, kFeature, lambda, t, Arrays.asList(new EvaluationMetricValue[]{new EvaluationMetricValue(metric, 0.0)}));
			results.add(r);
		}
		return results;
	}

	private Map<String,String> parseLine(String line){
		HashMap<String,String> map = new HashMap<>();
		String[] tokens = line.split(";");
		for(String token: tokens){
			String[] pair = token.split("=");
			map.put(pair[0], pair[1]);
		}
		map.put("last", tokens[tokens.length-1].split("=")[0]);
		return map;
	}

	public List<LCIFResult> runOuterLoop(File training, File test, List<LCIFResult> bestParameters) throws Exception{
		//do row-knn
		System.out.println("Outer loop");
		int maxKRow = bestParameters.stream().mapToInt((p) -> p.kRow).max().getAsInt();
		int maxKFeature = bestParameters.stream().mapToInt((p) -> p.kFeature).max().getAsInt();
		//init data
		LCIFData data = new LCIFData(training,test);
		data.printDataStats();
		//run predictions row based
		RowKNNCache cache = rowKNNSearch.computeAllSimilarRowsMultiThreaded(data, maxKRow);
		//run predictions feature based
		FeatureKNNCache cacheFeatures =  featureKNNSearch.computeAllFeatureLabelSimmilarities(data, maxKFeature);
		SecondRowKNNCache cacheSecondInstance = null;
		if(LCIF2_MODUS){
			cacheSecondInstance = secondRowKNNSearch.computeAllSimilarRows(data, maxKFeature);
		}
		//run linear combination
		List<LCIFResult> results = new ArrayList<>();
		for(LCIFResult parameter: bestParameters){
			LCIFPredictions predictionsRow;
			LCIFPredictions predictionsFeature;
			if(!LCIF2_MODUS){
				predictionsRow = rowKNNPredictions.makePredictionAllTestRows(data, cache, parameter.kRow);
				predictionsFeature = featureKNNPredictions.makePredictionAllTestRows(data, cacheFeatures, parameter.kFeature); 
			}
			else{
				predictionsRow = secondRowKNNPredictions.makePredictionAllTestRows(data, cache, cacheSecondInstance, parameter.kRow, parameter.kFeature);
				predictionsFeature = featureKNNPredictions.makePredictionAllTestRows(data, cacheFeatures, parameter.kFeature); 
			}
			LCIFPredictions predictionsCombo = lcifLinearCombination.makePredictionAllTestRows(data, predictionsRow, predictionsFeature, parameter.lambda);
			LCIFPredictions predictions = predictionsCombo.doOneThreshold(parameter.optimalTreshold);
			List<EvaluationMetricValue> values = evaluator.computeMetricValues(data, predictions);
			//testing: 
			evaluator.printPredictionsWithNoLabels(data, predictions);
			//only k
			values = values.stream().filter((met) -> met.name.equals(parameter.evaluationValues.get(0).name)).collect(Collectors.toList());
			results.add(new LCIFResult(parameter.kRow, parameter.kFeature, parameter.lambda, parameter.optimalTreshold, values));
		}
		return results;
	}
}
