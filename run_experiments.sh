mvn clean install -DskipTests=True
#large datasets
mvn test -Dtest=be.uantwerpen.experiments.large.TestCompareMethodsOnMedical1kDataset >> output-medical1k.log 
#mvn test -Dtest=be.uantwerpen.experiments.large.TestCompareMethodsOnCorel5kDataset >> output-corel5k.log
#mvn test -Dtest=be.uantwerpen.experiments.large.TestCompareMethodsOnBibtex7kDataset >> output-bibtex6k.log
#mvn test -Dtest=be.uantwerpen.experiments.large.TestCompareMethodsOnDelicious16kDataset >> output-delicious1k.log
#mvn test -Dtest=be.uantwerpen.experiments.large.TestCompareMethodsOnIMDB120kDataset >> output-imdb120k.log

#extreme datasets
#mvn test -Dtest=be.uantwerpen.experiments.extreme.TestExtremeWiki10k >> output-wiki10k.log
#mvn test -Dtest=be.uantwerpen.experiments.extreme.TestExtremeWikiLSHTC2300K >> output-wikilshtc.log
#or: 
#mvn test -Dtest=be.uantwerpen.experiments.extreme.TestExtremeWikiLSHTC2300K#runFeatureKnnNewSearch >> output-wikilshtc-feature-knn-new.log
#mvn test -Dtest=be.uantwerpen.experiments.extreme.TestExtremeWikiLSHTC2300K#runRowKnn >> output-wikilshtc-runrowknn.log
#mvn test -Dtest=be.uantwerpen.experiments.extreme.TestExtremeWikiLSHTC2300K#runFeatureKnn >> output-wikilshtc-featureknn.log
#mvn test -Dtest=be.uantwerpen.experiments.extreme.TestExtremeWikiLSHTC2300K#runLCIF >> runLCIF-wikilshtc-lcif.log
